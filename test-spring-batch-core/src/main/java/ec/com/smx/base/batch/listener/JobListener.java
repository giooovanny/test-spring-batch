package ec.com.smx.base.batch.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.stereotype.Component;

/**
 * Job Listener.
 *
 * @author gcadena on 19/3/20.
 * @version 1.0
 * @since 1.
 */
@Component
@Slf4j
public class JobListener extends JobExecutionListenerSupport {

    @Override
    public void afterJob(JobExecution jobExecution) {
        if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
           log.info("FINALIZÓ EL JOB!! Verifica los resultados:");
        }
    }
}
