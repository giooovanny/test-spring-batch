package ec.com.smx.base.repository;

import ec.com.smx.base.entity.PersonEntityTest;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;
import ec.com.kruger.spring.orm.jpa.repository.JPAQueryDslBaseRepository;

/**
 * Person repository compile with JPA and QUERYDSL.
 *
 * @author gcadena on 26/03/21.
 * @version 1.0
 * @since 1.0.0
 */
@Lazy
@Repository
public class PersonTestRepository extends JPAQueryDslBaseRepository<PersonEntityTest> implements
        IPersonTestRepository {


    /**
     * Constructor.
     */
    public PersonTestRepository() {
        super(PersonEntityTest.class);
    }

    /**
     * Save persons test.
     *
     * @param personEntityTest new Person test
     * @return A person test object
     */
    @Override
    public PersonEntityTest savePerson(PersonEntityTest personEntityTest) {
        this.save(personEntityTest);
        return personEntityTest;
    }

}
