package ec.com.smx.base.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Person entity Test.
 *
 * @author gcadena on 23/3/20.
 * @version 1.0
 * @since 1.
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "SSPCOTPERSONATEST")
public class PersonEntityTest {

    private static final long serialVersionUID = -1309767405452739579L;

    @Id
    @Column(name = "CODIGOPERSONATEST", nullable = false)
    private Integer codePerson;

    @Column(name = "TIPODOCUMENTO")
    private String identifyType;

    @Column(name = "NUMERODOCUMENTO")
    private String identifyNumber;

    @Column(name = "PRIMERNOMBRE")
    private String firstName;

    @Column(name = "SEGUNDONOMBRE")
    private String lastName;

    @Column(name = "NOMBRECOMPLETO")
    private String fullName;

    @Column(name = "ESTADOPERSONATEST")
    private String statusString;

    @Column(name = "GENEROPERSONATEST")
    private String gender;

    @Override
    public String toString() {
        return "PersonEntityTest{" +
                " identifyType='" + identifyType + '\'' +
                ", identifyNumber='" + identifyNumber + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", fullName='" + fullName + '\'' +
                ", statusString='" + statusString + '\'' +
                ", gender='" + gender + '\'' +
                '}';
    }
}
