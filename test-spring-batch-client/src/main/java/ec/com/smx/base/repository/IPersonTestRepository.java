package ec.com.smx.base.repository;

import ec.com.kruger.spring.orm.repository.IQueryDslBaseRepository;
import ec.com.smx.base.entity.PersonEntityTest;

/**
 * Person test repository specification.
 *
 * @author gcadena on 23/3/20.
 * @version 1.0
 * @since 1.
 */
public interface IPersonTestRepository extends IQueryDslBaseRepository<PersonEntityTest> {

    /**
     * Save persons test.
     *
     * @param personEntityTest new Person test
     * @return A person test object
     */
    PersonEntityTest savePerson(PersonEntityTest personEntityTest);
}

