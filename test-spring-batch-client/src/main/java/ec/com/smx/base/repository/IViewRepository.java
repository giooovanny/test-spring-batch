package ec.com.smx.base.repository;

import java.util.List;
import ec.com.smx.base.entity.UserViewSample;

/**
 * View repository specification.
 */
public interface IViewRepository {

    /**
     * Find user view by first name.
     *
     * @param firstName The first name.
     * @return An array of UserViewSample.
     */
    List<UserViewSample> findByFirstName(String firstName);

}
