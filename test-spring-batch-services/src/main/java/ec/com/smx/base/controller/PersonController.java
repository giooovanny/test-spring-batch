package ec.com.smx.base.controller;

import java.util.List;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.core.JsonProcessingException;
import ec.com.kruger.security.sso.springboot2.util.SecurityKeycloakUtil;
import ec.com.kruger.spring.ws.controller.BaseController;
import ec.com.smx.base.entity.PersonEntity;
import ec.com.smx.base.service.IPersonService;
import ec.com.smx.base.vo.FindPersonRequest;
import ec.com.smx.base.vo.FindPersonResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * Class to controller person rest services.
 *
 * @author klopez on 2/21/19.
 * @version 1.0
 * @since 1.0.0
 */
@RestController
@RequestMapping("/api/v1/person")
@Lazy
@Slf4j
@Tag(name = "Person", description = "The Person API")
public class PersonController extends BaseController {

    @Lazy
    @Autowired
    @Getter
    private IPersonService personService;

    /**
     * Find person by some prefefields.
     *
     * @deprecated As of new version response on /api/v2/person, because limit attributes of payload.
     * @param person The person filter
     * @return Array of {@link FindPersonResponse}
     */
    @PostMapping(path = "/findPersons",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Get Persons by criteria")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Found Persons",
            content = { @Content(mediaType = "application/json",
                schema = @Schema(implementation = FindPersonResponse.class)) }),
        @ApiResponse(responseCode = "400", description = "Invalid filter",
            content = @Content),
        @ApiResponse(responseCode = "404", description = "Persons not found",
            content = @Content) })
    @Deprecated
    public List<FindPersonResponse> findPersons(@RequestBody FindPersonRequest person) {
        return this.personService.findPersons(person);
    }

    /**
     * Find persons by first name.
     *
     * @param firstName The first name to filter
     * @return Array of {@link PersonEntity}
     */
    @Secured("ROLE_SEARCH_PERSON")
    @PreAuthorize("hasRole('ROLE_SEARCH_PERSON')")
    @GetMapping("/findPersonsByFirstName")
    public List<PersonEntity> findPersonsByFirstName(@RequestParam String firstName)
        throws JsonProcessingException {
        log.info("CurrentUserLogin: {}",
            objectMapper.writeValueAsString(SecurityKeycloakUtil.getCurrentUserLogin()));
        return personService.findByFirstName(firstName);
    }

}
