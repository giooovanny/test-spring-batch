package ec.com.smx.base.service;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import ec.com.kruger.spring.service.IBaseService;
import ec.com.smx.base.entity.PersonEntity;
import ec.com.smx.base.vo.FindPersonRequest;
import ec.com.smx.base.vo.FindPersonResponse;
import ec.com.smx.base.vo.FindPersonResponseV2;

/**
 * Person services specification.
 *
 * @author klopez on 2019/2/20.
 * @version 1.0
 * @since 1.0.0
 */
public interface IPersonService extends IBaseService<PersonEntity> {

    /**
     * Find persons by first name.
     *
     * @param firstName The first name
     * @return An array of person
     */
    List<PersonEntity> findByFirstName(@NotEmpty String firstName);

    /**
     * Find persons by some filter.
     *
     * @param personRequest The person data
     * @return An array of person
     */
    List<FindPersonResponse> findPersons(@Valid FindPersonRequest personRequest);

    /**
     * Save person with an identity number validation.
     *
     * @param personEntity The entity to save
     */
    void saveWithValidation(@NotNull PersonEntity personEntity);

    /**
     * Find persons as simple data by some filter.
     *
     * @param personRequest The person data
     * @return An List of person
     */
    List<FindPersonResponseV2> findSimplePersons(@Valid FindPersonRequest personRequest);
}
