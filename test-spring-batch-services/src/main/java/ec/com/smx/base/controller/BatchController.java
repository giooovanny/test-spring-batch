package ec.com.smx.base.controller;

import ec.com.smx.base.service.IBatchService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Class to controller batch rest services.
 *
 * @author gcadena on 26/03/21.
 * @version 1.0
 * @since 1.0.0
 */
@RestController
@RequestMapping("/api/v1/batch")
@Lazy
@Tag(name = "Batch", description = "The Batch API")
public class BatchController {

    @Lazy
    @Autowired
    private IBatchService batchService;

    /**
     * Init batch process.
     *
     */
    @GetMapping(path = "/processPerson")
    public void processPerson() {
        this.batchService.executeJob();
    }
}
