package ec.com.smx.base.batch.step;


import ec.com.smx.base.batch.processor.PersonItemProcessor;
import ec.com.smx.base.batch.reader.ExchangeReader;
import ec.com.smx.base.batch.writer.TransactionWriter;
import ec.com.smx.base.common.BaseConstants;
import ec.com.smx.base.vo.batch.PersonFileLine;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

/**
 * Steps for batch processes.
 *
 * @author gcadena on 23/3/20.
 * @version 1.0
 * @since 1.
 */
@Lazy
@Configuration
public class BaseBatchSteps {

    private static final Integer CHUNK_SIZE = 6;

    @Lazy
    @Autowired
    private StepBuilderFactory steps;

    /**
     * Step for save personctest from file to DB.
     *
     * @param baseJR Job repo.
     * @param exchangeReader Reader.
     * @param transactionWriter Writer.
     * @return Step to execute.
     */
    @Bean
    @Lazy
    public Step loadExchangeStep(@Qualifier("baseJR") JobRepository baseJR,
                                 ExchangeReader exchangeReader,
                                 TransactionWriter transactionWriter) {
        return this.steps.get(BaseConstants.STEP_EXCHANGE_MIGRATION).repository(baseJR)
                .<PersonFileLine, PersonFileLine>chunk(CHUNK_SIZE)
                .reader(exchangeReader)
                .processor(processor())
                .writer(transactionWriter)
                .build();
    }

    @Bean
    @Lazy
    @StepScope
    public PersonItemProcessor processor() {
        return new PersonItemProcessor();
    }

}
