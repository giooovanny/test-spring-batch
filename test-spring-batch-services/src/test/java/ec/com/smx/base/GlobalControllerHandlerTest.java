package ec.com.smx.base;

import ec.com.smx.base.common.GlobalControllerExceptionHandler;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class GlobalControllerHandlerTest {

    private GlobalControllerExceptionHandler globalControllerExceptionHandler;

    @Before
    public void setup() {
        this.globalControllerExceptionHandler = new GlobalControllerExceptionHandler();
    }

    @Test
    public void handleExcepctionTest(){
        //Act
        ResponseEntity<String> response = this.globalControllerExceptionHandler
            .handleConnversion(new UnsupportedOperationException());

        //Assert
        Assert.assertEquals("Messages are not equal", HttpStatus.BAD_REQUEST, response.getStatusCode());
    }
}
