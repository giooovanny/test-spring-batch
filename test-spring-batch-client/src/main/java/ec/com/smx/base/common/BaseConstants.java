package ec.com.smx.base.common;

/**
 * Class BaseConstants.
 *
 * @author gcadena on 23/3/20.
 * @version 1.0
 * @since 1.
 */
public class BaseConstants {

    /**
     * Job constants.
     */
    public static final String JOB_LAUNCHER = "baseJobLauncher";
    public static final String JOB_EXCHANGE_FILES = "exchangeFilesJob";


    /**
     * Step constants.
     */
    public static final String STEP_EXCHANGE_MIGRATION = "loadExchangeStep";


    /**
     * Reader constants.
     */
    public static final String DEFAULT_SEPARATOR = ",";
}
