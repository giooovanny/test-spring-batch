package ec.com.smx.base.vo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import ec.com.kruger.validation.javax.constraint.Cedula;
import ec.com.kruger.validation.javax.constraint.Ruc;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Class VO for request find person process.
 *
 * @author klopez on 2019/2/20.
 * @version 1.0
 * @since 1.0.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FindPersonRequest {

    private Integer codePerson;

    @NotBlank(message = "Name firstName is mandatory")
    @Size(min = 0, max = 50, message = "FirstName must be between {min} and {max} characters long")
    private String firstName;

    private String lastName;

    private String fullName;

    @Cedula
    private String ci;

    @Ruc
    private String ruc;
}
