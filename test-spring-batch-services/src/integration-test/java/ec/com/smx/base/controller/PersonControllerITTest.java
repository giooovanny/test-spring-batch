package ec.com.smx.base.controller;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.UUID;

import org.jose4j.jwk.JsonWebKeySet;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.NumericDate;
import org.jose4j.lang.JoseException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.google.common.collect.ImmutableMap;

import ec.com.kruger.spring.ws.common.WsConstants;
import ec.com.smx.base.BaseSpringBootApplication;
import ec.com.smx.base.service.IPersonService;
import ec.com.smx.base.vo.FindPersonRequest;
import ec.com.smx.base.vo.FindPersonResponse;

/**
 * Tests para servicios rest {@link PersonController} usando {@link MockMvc}.
 *
 * @author klopez
 * @version 1.0
 * @since 1.0.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BaseSpringBootApplication.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
@EnableTransactionManagement
@AutoConfigureMockMvc
@AutoConfigureWireMock(port = 0)
// random port, that is wired into properties with key wiremock.server.port
// @Sql("/import.sql")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class PersonControllerITTest {

    private static boolean testSetupIsCompleted = false;
    private static RsaJsonWebKey rsaJsonWebKey;

    @Autowired
    protected MockMvc mockMvc;
    @Autowired
    protected ObjectMapper objectMapper;

    @Value("${wiremock.server.baseUrl}")
    protected String keycloakBaseUrl;

    @Value("${keycloak.realm}")
    protected String keycloakRealm;

    @Autowired
    protected IPersonService personServices;

    private Faker faker = new Faker();
    private String firstNameMock = faker.name().firstName();
    private String lastNameMock = faker.name().lastName();
    private FindPersonResponse personResponse1 = FindPersonResponse.builder().codePerson(1)
        .firstName(firstNameMock).lastName(lastNameMock).build();
    private FindPersonResponse personResponse2 = FindPersonResponse.builder().codePerson(2)
        .firstName(faker.name().firstName())
        .lastName(faker.name().lastName()).build();
    private FindPersonResponse personResponse3 = FindPersonResponse.builder().codePerson(2)
        .firstName(faker.name().firstName()).lastName(lastNameMock).build();
    private FindPersonResponse personResponse4 = FindPersonResponse.builder().codePerson(2)
        .firstName(faker.name().firstName()).lastName(lastNameMock).build();

    private String generateJWT(String... roles) throws JoseException {

        // Create the Claims, which will be the content of the JWT
        JwtClaims claims = new JwtClaims();
        claims.setJwtId(UUID.randomUUID().toString()); // a unique identifier for the token
        claims.setExpirationTimeMinutesInTheFuture(
            10); // time when the token will expire (10 minutes from now)
        claims.setNotBeforeMinutesInThePast(
            0); // time before which the token is not yet valid (2 minutes ago)
        claims.setIssuedAtToNow(); // when the token was issued/created (now)
        claims.setAudience("account"); // to whom this token is intended to be sent
        claims.setIssuer(String.format("%s/auth/realms/%s", keycloakBaseUrl,
            keycloakRealm)); // who creates the token and signs it
        claims.setSubject(
            UUID.randomUUID().toString()); // the subject/principal is whom the token is about
        claims.setClaim("typ", "Bearer"); // set type of token
        claims.setClaim("azp",
            "example-client-id"); // Authorized party (the party to which this token was issued)
        claims.setClaim("auth_time",
            NumericDate.fromMilliseconds(Instant.now().minus(11, ChronoUnit.SECONDS).toEpochMilli())
                .getValue()); // time when
        // authentication
        // occured
        claims.setClaim("session_state", UUID.randomUUID().toString()); // keycloak specific ???
        claims.setClaim("acr", "0"); // Authentication context class
        claims.setClaim("realm_access", ImmutableMap.of("roles",
            Arrays.asList("offline_access", "uma_authorization", "user"))); // keycloak roles
        claims.setClaim("resource_access",
            ImmutableMap.of("account", ImmutableMap.of("roles",
                Arrays.asList("manage-account", "manage-account-links", "view-profile")),
                "APP-BASE",
                ImmutableMap.of("roles", Arrays.asList(roles)))

        ); // keycloak roles
        claims.setClaim("scope", "profile email");
        claims.setClaim("name",
            "John Doe"); // additional claims/attributes about the subject can be added
        claims.setClaim("email_verified", true);
        claims.setClaim("preferred_username", "doe.john");
        claims.setClaim("given_name", "John");
        claims.setClaim("family_name", "Doe");

        claims.setClaim("profile",
            "{\"companyID\":1, \"codeFun\":1}, \"userID\":\"FRM0\", \"defaultProfile\":730}");

        // A JWT is a JWS and/or a JWE with JSON claims as the payload.
        // In this example it is a JWS so we create a JsonWebSignature object.
        JsonWebSignature jws = new JsonWebSignature();

        // The payload of the JWS is JSON content of the JWT Claims
        jws.setPayload(claims.toJson());

        // The JWT is signed using the private key
        jws.setKey(rsaJsonWebKey.getPrivateKey());

        // Set the Key ID (kid) header because it's just the polite thing to do.
        // We only have one key in this example but a using a Key ID helps
        // facilitate a smooth key rollover process
        jws.setKeyIdHeaderValue(rsaJsonWebKey.getKeyId());

        // Set the signature algorithm on the JWT/JWS that will integrity protect the
        // claims
        jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);

        // set the type header
        jws.setHeader("typ", "JWT");

        // Sign the JWS and produce the compact serialization or the complete JWT/JWS
        // representation, which is a string consisting of three dot ('.') separated
        // base64url-encoded parts in the form Header.Payload.Signature
        return jws.getCompactSerialization();
    }

    /**
     * Setup de los mock.
     * <p>
     * Nota: Se llama al setUpBase para inicializar el contexto solo con el controlador que se desea
     * probar.
     * </p>
     *
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     */
    @Before
    public void setUp() throws JoseException {
        // personServices.save(personResponse1);
        // personServices.save(personResponse2);
        // personServices.save(personResponse3);
        // personServices.save(personResponse4);
        /*
         * personResponse4.setStatus(true); personResponse4.setCreatedFromIp("some");
         * personResponse4.setCompanyCode(1); personResponse4.setCreatedById("FRM0");
         */
        if (!testSetupIsCompleted) {
            // Generate an RSA key pair, which will be used for signing and verification of
            // the JWT, wrapped in a JWK
            rsaJsonWebKey = RsaJwkGenerator.generateJwk(2048);
            rsaJsonWebKey.setKeyId("k1");
            rsaJsonWebKey.setAlgorithm(AlgorithmIdentifiers.RSA_USING_SHA256);
            rsaJsonWebKey.setUse("sig");

            String openidConfig =
                "{\n" + "  \"issuer\": \"" + keycloakBaseUrl + "/auth/realms/" + keycloakRealm
                    + "\",\n" + "  \"authorization_endpoint\": \""
                    + keycloakBaseUrl + "/auth/realms/" + keycloakRealm
                    + "/protocol/openid-connect/auth\",\n" + "  \"token_endpoint\": \""
                    + keycloakBaseUrl
                    + "/auth/realms/" + keycloakRealm + "/protocol/openid-connect/token\",\n"
                    + "  \"token_introspection_endpoint\": \"" + keycloakBaseUrl
                    + "/auth/realms/" + keycloakRealm
                    + "/protocol/openid-connect/token/introspect\",\n"
                    + "  \"userinfo_endpoint\": \"" + keycloakBaseUrl
                    + "/auth/realms/" + keycloakRealm + "/protocol/openid-connect/userinfo\",\n"
                    + "  \"end_session_endpoint\": \"" + keycloakBaseUrl
                    + "/auth/realms/" + keycloakRealm + "/protocol/openid-connect/logout\",\n"
                    + "  \"jwks_uri\": \"" + keycloakBaseUrl + "/auth/realms/"
                    + keycloakRealm + "/protocol/openid-connect/certs\",\n"
                    + "  \"check_session_iframe\": \"" + keycloakBaseUrl + "/auth/realms/"
                    + keycloakRealm
                    + "/protocol/openid-connect/login-status-iframe.html\",\n"
                    + "  \"registration_endpoint\": \"" + keycloakBaseUrl + "/auth/realms/"
                    + keycloakRealm + "/clients-registrations/openid-connect\",\n"
                    + "  \"introspection_endpoint\": \"" + keycloakBaseUrl + "/auth/realms/"
                    + keycloakRealm + "/protocol/openid-connect/token/introspect\"\n" + "}";
            stubFor(WireMock.get(urlEqualTo(
                String.format("/auth/realms/%s/.well-known/openid-configuration", keycloakRealm)))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                    .withBody(openidConfig)));
            stubFor(WireMock.get(urlEqualTo(
                String.format("/auth/realms/%s/protocol/openid-connect/certs", keycloakRealm)))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                    .withBody(new JsonWebKeySet(rsaJsonWebKey).toJson())));

            testSetupIsCompleted = true;
        }
        // ScriptUtils.executeSqlScript
    }

    /**
     * Test find person
     *
     * @throws Exception
     */
    @Test
    public void testFindPerson() throws Exception {
        FindPersonRequest personRequest = FindPersonRequest.builder().firstName("Kenny")
            .lastName("Lopez").build();
        mockMvc
            .perform(
                post("/baseServices/api/v1/person/findPersons").contextPath("/baseServices")
                    .header("Authorization", String.format("Bearer %s", generateJWT()))
                    .content(objectMapper.writeValueAsString(personRequest))
                    .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk()).andExpect(header()
            .string(WsConstants.REST_TYPE_HEADER, is(WsConstants.ResponseType.SUCCESS.toString())))
            .andExpect(jsonPath("$", hasSize(1))).andExpect(jsonPath("$[0].lastName", is("Lopez")))
            // .andExpect(jsonPath("$[1].lastName", is("Lopez")))
            // .andExpect(jsonPath("$[2].lastName", is("Lopez")))
            .andDo(MockMvcResultHandlers.print());
    }

    /**
     * Test find person by first name
     *
     * @throws Exception
     */
    @Test
    public void testFindPersonsByFirstName() throws Exception {
        mockMvc
            .perform(
                get("/baseServices/api/v1/person/findPersonsByFirstName").contextPath("/baseServices")
                    .param("firstName", "Kenny")
                    .header("Authorization",
                        String.format("Bearer %s", generateJWT("ROLE_SEARCH_PERSON")))
                    .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk()).andExpect(header()
            .string(WsConstants.REST_TYPE_HEADER, is(WsConstants.ResponseType.SUCCESS.toString())))
            .andExpect(jsonPath("$", hasSize(1))).andExpect(jsonPath("$[0].firstName", is("Kenny")))
            .andDo(MockMvcResultHandlers.print());
    }

    /**
     * Test find person by first name unautorized.
     *
     * @throws Exception
     */
    @Test
    public void testFindPersonsByFirstNameUnautorized() throws Exception {
        mockMvc
            .perform(
                get("/baseServices/api/v1/person/findPersonsByFirstName").contextPath("/baseServices")
                    .param("firstName", "Kenny")
                    .header("Authorization", String.format("Bearer %s", generateJWT()))
                    .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().is5xxServerError()).andExpect(header()
            .string(WsConstants.REST_TYPE_HEADER, is(WsConstants.ResponseType.ERROR.toString())));
    }

    /**
     * Test find person by first name with empty required param
     *
     * @throws Exception
     */
    @Test
    public void testFindPersonsByFirstNameMissingParam() throws Exception {
        mockMvc
            .perform(
                get("/baseServices/api/v1/person/findPersonsByFirstName").contextPath("/baseServices")
                    .header("Authorization", String.format("Bearer %s", generateJWT()))
                    .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().is5xxServerError()).andExpect(header()
            .string(WsConstants.REST_TYPE_HEADER, is(WsConstants.ResponseType.ERROR.toString())));
    }
}
