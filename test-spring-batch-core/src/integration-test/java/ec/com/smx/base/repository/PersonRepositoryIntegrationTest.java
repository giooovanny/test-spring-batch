package ec.com.smx.base.repository;

import static org.hamcrest.CoreMatchers.containsStringIgnoringCase;
import static org.hamcrest.CoreMatchers.endsWithIgnoringCase;
import static org.hamcrest.CoreMatchers.startsWithIgnoringCase;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import java.util.Arrays;
import java.util.List;

import org.hamcrest.Matchers;
import org.hamcrest.beans.HasPropertyWithValue;
import org.hamcrest.core.Every;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import com.github.javafaker.Faker;

import ec.com.kruger.spring.orm.dto.SearchModelDTO;
import ec.com.smx.base.entity.PersonEntity;
import ec.com.smx.base.vo.FindPersonRequest;
import ec.com.smx.base.vo.FindPersonResponse;

/**
 * Test for PersonRepository class.
 *
 * @author klopez on 11/06/2020.
 * @version 1.0
 * @since 1.0.0
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class PersonRepositoryIntegrationTest {

    // Arrange
    String act = "ACT";
    String ina = "INA";
    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private IPersonRepository repository;
    private Faker faker = new Faker();
    // Arrange
    String firstName = faker.name().firstName();
    String lastName = faker.name().lastName();
    String fullName = faker.name().fullName();
    String identity = faker.number().digits(10);

    String initName = faker.name().firstName();
    String endName = faker.name().firstName();

    @Before
    public void setup() {
        // Arrange
        // Exist 3 person in db inserted by scripts

        // person1
        entityManager
            .persist(PersonEntity.builder().identifyNumber(identity).firstName(firstName)
                .lastName(lastName).fullName(fullName).statusString(act).build());

        // person2 ina
        entityManager.persist(PersonEntity.builder().identifyNumber(faker.number().digits(10))
            .firstName(faker.name().firstName())
            .lastName(faker.name().lastName()).fullName(faker.name().fullName()).statusString(ina)
            .build());

        // person3
        entityManager.persist(PersonEntity.builder().identifyNumber(faker.number().digits(10))
            .firstName(faker.name().firstName())
            .lastName(faker.name().lastName()).fullName(faker.name().fullName()).statusString(act)
            .build());

        // person4 ina
        entityManager.persist(PersonEntity.builder().identifyNumber(faker.number().digits(10))
            .firstName(faker.name().firstName())
            .lastName(faker.name().lastName()).fullName(faker.name().fullName()).statusString(ina)
            .build());

        // person5
        entityManager.persist(PersonEntity.builder().identifyNumber(faker.number().digits(10))
            .firstName(initName + " " + firstName)
            .lastName(initName + " " + faker.name().lastName())
            .fullName(initName + " " + faker.name().fullName()).statusString(act).build());

        // person6
        entityManager.persist(PersonEntity.builder().identifyNumber(faker.number().digits(10))
            .firstName(firstName + " " + endName)
            .lastName(faker.name().lastName() + " " + endName)
            .fullName(faker.name().fullName() + " " + endName).statusString(act).build());

        // person7 ina
        entityManager.persist(
            PersonEntity.builder().identifyNumber(faker.number().digits(10)).firstName(firstName)
                .lastName(faker.name().lastName())
                .fullName(faker.name().fullName()).statusString(ina).build());

        // person8 ina
        entityManager.persist(
            PersonEntity.builder().identifyNumber(faker.number().digits(10)).firstName(firstName)
                .lastName(faker.name().lastName())
                .fullName(faker.name().fullName()).statusString(ina).build());

        entityManager.flush();
    }

    @Test
    public void testFindByFirstNameEmptyList() {
        // Act (object under test)
        List<PersonEntity> personEntityList = repository.findByFirstName(faker.name().firstName());

        // Assert
        Assert.assertTrue("Empty collection", personEntityList.isEmpty());
    }

    @Test
    public void testFindByContainFirstNameAndActive() {
        // Act (object under test)
        List<PersonEntity> personEntityList = repository.findByFirstName(firstName);

        // Assert
        assertThat(personEntityList, Matchers.hasSize(3));

        // Way 1
        assertThat(personEntityList.get(0).getFirstName(), containsStringIgnoringCase(firstName));
        assertThat(personEntityList.get(0).getStatusString(), is(act));

        assertThat(personEntityList.get(1).getFirstName(), containsStringIgnoringCase(firstName));
        assertThat(personEntityList.get(1).getStatusString(), is(act));

        assertThat(personEntityList.get(2).getFirstName(), containsStringIgnoringCase(firstName));
        assertThat(personEntityList.get(2).getStatusString(), is(act));

        // Way 2
        assertThat(personEntityList, (Every.everyItem(
            HasPropertyWithValue.hasProperty("firstName", containsStringIgnoringCase(firstName)))));
        assertThat(personEntityList,
            (Every.everyItem(HasPropertyWithValue.hasProperty("statusString", is(act)))));

        // Way 3
        personEntityList.forEach(person -> Assert
            .assertThat(person.getFirstName(), containsStringIgnoringCase(firstName)));
        personEntityList.forEach(person -> Assert.assertThat(person.getStatusString(), is(act)));

        // Way 4
        personEntityList.forEach(person -> {
            Assert.assertThat(person.getFirstName(), containsStringIgnoringCase(firstName));
            Assert.assertThat(person.getStatusString(), is(act));
        });
    }

    @Test
    public void testFindPersonsWithPartialInitFirstNameAndActive() {
        // Arrange

        // Act (object under test)
        List<PersonEntity> personEntityList = repository.findByFirstName(initName);

        // Assert
        assertThat(personEntityList, Matchers.hasSize(1));

        personEntityList.forEach(person -> {
            Assert.assertThat(person.getFirstName(), startsWithIgnoringCase(initName));
            Assert.assertThat(person.getStatusString(), is(act));
        });
    }

    @Test
    public void testFindPersonsWithPartialEndFirstNameAndActive() {
        // Arrange

        // Act (object under test)
        List<PersonEntity> personEntityList = repository.findByFirstName(endName);

        // Assert
        assertThat(personEntityList, Matchers.hasSize(1));

        personEntityList.forEach(person -> {
            Assert.assertThat(person.getFirstName(), endsWithIgnoringCase(endName));
            Assert.assertThat(person.getStatusString(), is(act));
        });
    }

    @Test
    public void testFindPersonsWithNoFilterReturnOnlyActive() {
        // Arrange
        FindPersonRequest expectedPersonRequest = FindPersonRequest.builder().build();

        // Act (object under test)
        List<FindPersonResponse> personResponseList = repository.findPersons(expectedPersonRequest);

        // Assert
        assertThat(personResponseList, Matchers.hasSize(7));
    }

    @Test
    public void testFindPersonsByCode() {
        // Arrange
        Integer codePerson = 1;
        FindPersonRequest expectedPersonRequest = FindPersonRequest.builder().codePerson(1).build();

        // Act (object under test)
        List<FindPersonResponse> personResponseList = repository.findPersons(expectedPersonRequest);

        // Assert
        assertThat(personResponseList, Matchers.hasSize(1));
        personResponseList
            .forEach(person -> Assert.assertThat(person.getCodePerson(), is(codePerson)));
    }

    @Test
    public void testFindPersonsContainFullName() {
        // Arrange
        FindPersonRequest expectedPersonRequest = FindPersonRequest.builder().fullName(fullName)
            .build();

        // Act (object under test)
        List<FindPersonResponse> personResponseList = repository.findPersons(expectedPersonRequest);

        // Assert
        assertThat(personResponseList, Matchers.hasSize(1));
        personResponseList.forEach(person -> Assert
            .assertThat(person.getFullName(), containsStringIgnoringCase(fullName)));
    }

    @Test
    public void testFindPersonsByInitialPartFullName() {
        // Arrange
        FindPersonRequest expectedPersonRequest = FindPersonRequest.builder().fullName(initName)
            .build();

        // Act (object under test)
        List<FindPersonResponse> personResponseList = repository.findPersons(expectedPersonRequest);

        // Assert
        assertThat(personResponseList, Matchers.hasSize(1));
        personResponseList.forEach(
            person -> Assert.assertThat(person.getFullName(), startsWithIgnoringCase(initName)));
    }

    @Test
    public void testFindPersonsByEndPartFullName() {
        // Arrange
        FindPersonRequest expectedPersonRequest = FindPersonRequest.builder().fullName(endName)
            .build();

        // Act (object under test)
        List<FindPersonResponse> personResponseList = repository.findPersons(expectedPersonRequest);

        // Assert
        assertThat(personResponseList, Matchers.hasSize(1));
        personResponseList.forEach(
            person -> Assert.assertThat(person.getFullName(), endsWithIgnoringCase(endName)));
    }

    @Test
    public void testFindPersonsContainFirstName() {
        // Arrange
        FindPersonRequest expectedPersonRequest = FindPersonRequest.builder().firstName(firstName)
            .build();

        // Act (object under test)
        List<FindPersonResponse> personResponseList = repository.findPersons(expectedPersonRequest);

        // Assert
        assertThat(personResponseList, Matchers.hasSize(3));
        personResponseList.forEach(person -> Assert
            .assertThat(person.getFirstName(), containsStringIgnoringCase(firstName)));
    }

    @Test
    public void testFindPersonsByInitialPartFirstName() {
        // Arrange
        FindPersonRequest expectedPersonRequest = FindPersonRequest.builder().firstName(initName)
            .build();

        // Act (object under test)
        List<FindPersonResponse> personResponseList = repository.findPersons(expectedPersonRequest);

        // Assert
        assertThat(personResponseList, Matchers.hasSize(1));
        personResponseList.forEach(
            person -> Assert.assertThat(person.getFirstName(), startsWithIgnoringCase(initName)));
    }

    @Test
    public void testFindPersonsByEndPartFirstName() {
        // Arrange
        FindPersonRequest expectedPersonRequest = FindPersonRequest.builder().firstName(endName)
            .build();

        // Act (object under test)
        List<FindPersonResponse> personResponseList = repository.findPersons(expectedPersonRequest);

        // Assert
        assertThat(personResponseList, Matchers.hasSize(1));
        personResponseList.forEach(
            person -> Assert.assertThat(person.getFirstName(), endsWithIgnoringCase(endName)));
    }

    @Test
    public void testFindPersonsContainLastName() {
        // Arrange
        FindPersonRequest expectedPersonRequest = FindPersonRequest.builder().lastName(lastName)
            .build();

        // Act (object under test)
        List<FindPersonResponse> personResponseList = repository.findPersons(expectedPersonRequest);

        // Assert
        assertThat(personResponseList, Matchers.hasSize(1));
        personResponseList.forEach(person -> Assert
            .assertThat(person.getLastName(), containsStringIgnoringCase(lastName)));
    }

    @Test
    public void testFindPersonsByInitialPartLastName() {
        // Arrange
        FindPersonRequest expectedPersonRequest = FindPersonRequest.builder().lastName(initName)
            .build();

        // Act (object under test)
        List<FindPersonResponse> personResponseList = repository.findPersons(expectedPersonRequest);

        // Assert
        assertThat(personResponseList, Matchers.hasSize(1));
        personResponseList.forEach(
            person -> Assert.assertThat(person.getLastName(), startsWithIgnoringCase(initName)));
    }

    @Test
    public void testFindPersonsByEndPartLastName() {
        // Arrange
        FindPersonRequest expectedPersonRequest = FindPersonRequest.builder().lastName(endName)
            .build();

        // Act (object under test)
        List<FindPersonResponse> personResponseList = repository.findPersons(expectedPersonRequest);

        // Assert
        assertThat(personResponseList, Matchers.hasSize(1));
        personResponseList.forEach(
            person -> Assert.assertThat(person.getLastName(), endsWithIgnoringCase(endName)));
    }

    @Test
    public void testFindByIdentifyNumber() {
        // Act (object under test)
        PersonEntity personEntity = repository.findByIdentifyNumber(identity);

        // Assert
        Assert.assertNotNull("Invalid identify number", personEntity);
        Assert.assertNotNull(personEntity);

        assertThat(personEntity.getIdentifyNumber(), is(identity));

    }

    @Test
    public void testFindPaginated() {
        // Act (object under test)
        Page<PersonEntity> result = repository.findPagedData(PageRequest.of(0, 5));

        // Assert
        Assert.assertNotNull("Not null page", result);

        assertThat(result.getTotalElements(), is(7l));
        assertThat(result.getTotalPages(), is(2));
        assertThat(result.getNumberOfElements(), is(5));

        result.get().map(PersonEntity::getId).forEach(Assert::assertNotNull);
    }

    @Test
    public void testFindPaginatedWithGenericFilterEqual() {
        // Arrange
        int codePerson = 1;
        final SearchModelDTO<Integer> filter1 = SearchModelDTO.<Integer>builder()
            .dataType("integer").parameterPattern("codePerson").parameterValue(codePerson)
            .comparatorTypeEnum("Igual").selectedOption("uv").operatorOR(false).insensitive(false)
            .rangeBean(null).build();

        // Act (object under test)
        Page<PersonEntity> result = repository
            .findPagedDataWithGenericFilters(Arrays.asList(filter1), PageRequest.of(0, 2));

        // Assert
        Assert.assertNotNull("Not null page", result);
        assertThat(result.getTotalElements(), is(1l));
        Assert.assertEquals("Check total pages", 1, result.getTotalPages());
        Assert.assertEquals("Check elements of current page", 1, result.getNumberOfElements());

        assertThat(result.getContent().get(0).getId(), is(codePerson));

    }

    @Test
    public void testFindPaginatedWithGenericFilterIn() {
        // Arrange
        int codePerson1 = 1;
        int codePerson2 = 2;

        final SearchModelDTO<Integer> filter2 = SearchModelDTO.<Integer>builder()
            .dataType("integer").parameterPattern("codePerson")
            .parameterValues(Arrays.asList(codePerson1, codePerson2)).comparatorTypeEnum("Igual")
            .selectedOption("lv").operatorOR(false).insensitive(false)
            .rangeBean(null).build();

        // Act (object under test)
        Page<PersonEntity> result = repository
            .findPagedDataWithGenericFilters(Arrays.asList(filter2), PageRequest.of(0, 2));

        // Assert
        Assert.assertNotNull("Not null page", result);
        Assert.assertEquals("Check total result", 2, result.getTotalElements());
        Assert.assertEquals("Check total pages", 1, result.getTotalPages());
        Assert.assertEquals("Check elements of current page", 2, result.getNumberOfElements());

        assertThat(result.getContent().get(0).getId(), is(codePerson1));
        assertThat(result.getContent().get(1).getId(), is(codePerson2));

    }

    @Test
    public void testFindPaginatedWithGenericFilterInCollection() {
        // Arrange
        String functionaryCode1 = "1";

        final SearchModelDTO<String> filter2 = SearchModelDTO.<String>builder().dataType("string")
            .parameterPattern("functionaries.functionaryCode")
            .parameterValues(Arrays.asList(functionaryCode1)).comparatorTypeEnum("Igual")
            .selectedOption("lv").operatorOR(false).insensitive(false)
            .rangeBean(null).build();

        // Act (object under test)
        Page<PersonEntity> result = repository
            .findPagedDataWithGenericFilters(Arrays.asList(filter2), PageRequest.of(0, 2));

        // Assert
        Assert.assertNotNull("Not null page", result);
        Assert.assertEquals("Check total result", 0, result.getTotalElements());
        Assert.assertEquals("Check total pages", 0, result.getTotalPages());
        Assert.assertEquals("Check elements of current page", 0, result.getNumberOfElements());

    }
}
