package ec.com.smx.base.service;

import ec.com.smx.base.common.BaseConstants;
import ec.com.smx.base.exception.BatchException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.time.Instant;
import java.util.Date;

@Validated
@Lazy
@Service
@Slf4j
public class BatchService implements IBatchService{

    @Lazy
    @Autowired
    @Qualifier(BaseConstants.JOB_LAUNCHER)
    private SimpleJobLauncher baseJobLauncher;

    @Lazy
    @Autowired
    @Qualifier(BaseConstants.JOB_EXCHANGE_FILES)
    private Job exchangeFilesJob;


    /**
     * Add default JOB param and executes provided JOB with params.
     *
     */
    @Override
    public void executeJob() {
        try {
            Date d1 = Date.from(Instant.now());

            JobParametersBuilder executionParams = new JobParametersBuilder();
            executionParams.addString("file.input", "dat.csv");
            JobExecution jobExecution = baseJobLauncher.run(exchangeFilesJob,executionParams.toJobParameters());
            jobExecution.setStartTime(Date.from(Instant.now()));

            if (ExitStatus.FAILED.getExitCode()
                    .equals(jobExecution.getExitStatus().getExitCode())) {
                throw new BatchException("Batch Error. ");
            } else {
                log.info("--BATCH-LOG: End of batch process: start at:{}, ends at:{}", d1,
                                Date.from(Instant.now()));
            }
        } catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException exception) {
            throw new BatchException("Batch Error. ",exception);
        }
    }
}
