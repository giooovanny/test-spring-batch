package ec.com.smx.base.vo.batch;

import lombok.Data;

import javax.persistence.Column;

/**
 * Vo for transaction file lines.
 *
 * @author Bryan Vasquez.
 */
@Data
public class PersonFileLine {

    private Integer codePerson;

    private String identifyType;

    private String identifyNumber;

    private String firstName;

    private String lastName;

    private String fullName;

    private String statusString;

    private String gender;

    @Override
    public String toString() {
        return "PersonFileLine{" +
                "codePerson=" + codePerson +
                ", identifyType='" + identifyType + '\'' +
                ", identifyNumber='" + identifyNumber + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", fullName='" + fullName + '\'' +
                ", statusString='" + statusString + '\'' +
                ", gender='" + gender + '\'' +
                '}';
    }
}
