package ec.com.smx.base.service;

import ec.com.kruger.spring.service.IBaseService;
import ec.com.smx.base.entity.PersonEntityTest;


/**
 * Person services specification.
 *
 * @author gcadena on 23/3/20.
 * @version 1.0
 * @since 1.
 */
public interface IPersonTestService extends IBaseService<PersonEntityTest> {

    /**
     * Save persons test.
     *
     * @param personEntityTest new Person test
     * @return A person test object
     */
    void savePerson(PersonEntityTest personEntityTest);

}
