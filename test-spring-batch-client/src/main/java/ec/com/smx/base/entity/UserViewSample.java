package ec.com.smx.base.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

/**
 * Mapping to UserView Sample.
 *
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Immutable
@Table(name = "SSPCOVUSERS")
public class UserViewSample {

    /**
     * User identifier.
     */
    @Id
    private String userId;

    /**
     * User common name.
     */
    private String usercomnom;

}
