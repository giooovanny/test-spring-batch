package ec.com.smx.base.exception;

/**
 * Management Batch Exception.
 *
 * @author gcadena on 23/3/20.
 * @version 1.0
 * @since 1.
 */
public class BatchException extends RuntimeException{

    private static final long serialVersionUID = 3263046821289003399L;

    /**
     * Constructor.
     *
     */
    public BatchException() {
        super();
    }

    /**
     * Constructor with args.
     *
     * @param message The message
     * @param cause The cause
     */
    public BatchException(String message, Throwable cause) {
        super(message,
                cause);
    }

    /**
     * Constructor with args.
     *
     * @param message The message
     */
    public BatchException(String message) {
        super(message);
    }
}
