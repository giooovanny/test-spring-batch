package ec.com.smx.base.exception;

import org.junit.Assert;
import org.junit.Test;

/**
 * Test for custom exception IdentifyNumberException.
 *
 * @author jlage on 06/03/2019.
 * @version 1.0
 * @since 1.0.0
 */
public class IdentifyNumberExceptionTest {

    @Test
    public void testThrowExceptionWithValidMessage() {
        try {
            throw new IdentifyNumberException("Test custom message");
        } catch (IdentifyNumberException e) {
            Assert.assertEquals("Messages are not equal", "Test custom message", e.getMessage());
        }
    }
}
