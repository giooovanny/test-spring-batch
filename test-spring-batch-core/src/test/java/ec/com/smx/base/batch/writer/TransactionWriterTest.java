package ec.com.smx.base.batch.writer;

import ec.com.smx.base.config.BaseConfiguration;
import ec.com.smx.base.config.BatchConfiguration;
import ec.com.smx.base.vo.batch.PersonFileLine;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.test.AssertFile;
import org.springframework.batch.test.MetaDataInstanceFactory;
import org.springframework.batch.test.StepScopeTestUtils;
import org.springframework.batch.test.context.SpringBatchTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

/**
 * Test for TransactionWriter class.
 *
 * @author gcadena on 26/03/21.
 * @version 1.0
 * @since 1.0.0
 */
@SpringBatchTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {BaseConfiguration.class, BatchConfiguration.class})
public class TransactionWriterTest {

    @Autowired
    ItemWriter<PersonFileLine> itemWriter;

    private static final String TEST_OUTPUT = "src/test/resources/batch/actual-output.txt";

    private static final String EXPECTED_OUTPUT_ONE = "src/test/resources/batch/expected-output.txt";

    private JobParameters defaultJobParameters() {
        JobParametersBuilder paramsBuilder = new JobParametersBuilder();
        paramsBuilder.addString("file.output", TEST_OUTPUT);
        return paramsBuilder.toJobParameters();
    }

    @Test
    public void testStepWhenWriterCalledThenSuccess() throws Exception {

        // ARRANGE
        FileSystemResource expectedResult = new FileSystemResource(EXPECTED_OUTPUT_ONE);
        FileSystemResource actualResult = new FileSystemResource(TEST_OUTPUT);

        PersonFileLine personFileLine = new PersonFileLine();
        personFileLine.setCodePerson(584539568);
        personFileLine.setFirstName("Oscar");
        personFileLine.setLastName("Andrade");
        personFileLine.setFullName("Oscar Andrade");
        personFileLine.setIdentifyType("cedula");
        personFileLine.setIdentifyNumber("1727366088");
        personFileLine.setGender("masculino");
        personFileLine.setStatusString("soltero");

        StepExecution stepExecution = MetaDataInstanceFactory.createStepExecution(defaultJobParameters());

        // ACT
        StepScopeTestUtils.doInStepScope(stepExecution, () -> {
            itemWriter.write(Arrays.asList(personFileLine));
            return null;
        });

        // ASSERT
        AssertFile.assertFileEquals(expectedResult, actualResult);
    }
}
