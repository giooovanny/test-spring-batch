package ec.com.smx.base.repository;

import java.util.List;
import ec.com.smx.base.entity.UserViewSample;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ViewRepositoryTest {

    @Autowired
    private ViewRepository repository;

    @Test
    public void testFindByFirstNameEmptyList() {
        List<UserViewSample> personEntityList = repository.findByFirstName("ejemplo");
        Assert.assertTrue("Empty collection", !personEntityList.isEmpty());
    }
}
