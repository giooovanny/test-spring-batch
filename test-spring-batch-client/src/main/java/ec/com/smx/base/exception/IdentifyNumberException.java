package ec.com.smx.base.exception;

/**
 * Manage exception for existing entity with identify number.
 *
 * @author jlage on 01/03/2019.
 * @version 1.0
 * @since 1.0.0
 */
public class IdentifyNumberException extends RuntimeException {

    private static final long serialVersionUID = 1829167860648997589L;

    /**
     * Personalize exception for identify number validations.
     *
     * @param message A message for exception
     */
    public IdentifyNumberException(String message) {
        super(message);
    }
}
