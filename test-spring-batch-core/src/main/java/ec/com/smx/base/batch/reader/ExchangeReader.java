package ec.com.smx.base.batch.reader;

import ec.com.smx.base.batch.linemaper.PersonLineMapper;
import ec.com.smx.base.common.BaseConstants;
import ec.com.smx.base.vo.batch.PersonFileLine;
import lombok.Getter;
import lombok.Setter;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.file.transform.LineTokenizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Input migration file reader.
 *
 * @author gcadena on 23/3/20.
 * @version 1.0
 * @since 1.
 */
@Lazy
@Component
@StepScope
@Getter
@Setter
public class ExchangeReader extends FlatFileItemReader<PersonFileLine> {

    /**
     * Constructor.
     *
     *
     */
    public ExchangeReader(@Value("#{jobParameters['file.input']}") String fileInput) {
        super();
        try {
            super.setResource(new FileSystemResource((new ClassPathResource(fileInput)).getFile()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        super.setLinesToSkip(1);
        super.setEncoding("UTF-8");
        super.setLineMapper(createTransactionLineMapper());
        super.setStrict(false);
    }

    private static LineMapper<PersonFileLine> createTransactionLineMapper() {
        PersonLineMapper itemsLineMapper = new PersonLineMapper();
        LineTokenizer itemsLineTokenizer = createItemsLineTokenizer();
        itemsLineMapper.setLineTokenizer(itemsLineTokenizer);
        FieldSetMapper<PersonFileLine> informationMapper = createItemsInformationMapper();
        itemsLineMapper.setFieldSetMapper(informationMapper);
        return itemsLineMapper;
    }

    /**
     * Creates line tokenizer.
     *
     * @return Line tokenizer.
     */
    private static LineTokenizer createItemsLineTokenizer() {
        DelimitedLineTokenizer orderInputLineTokenizer = new DelimitedLineTokenizer();
        orderInputLineTokenizer.setDelimiter(BaseConstants.DEFAULT_SEPARATOR);
        orderInputLineTokenizer
            .setNames("identifyType", "identifyNumber", "firstName",
                    "lastName", "fullName", "statusString", "gender");
        return orderInputLineTokenizer;
    }

    /**
     * Item info mapper.
     *
     * @return Field mapper.
     */
    private static FieldSetMapper<PersonFileLine> createItemsInformationMapper() {
        BeanWrapperFieldSetMapper<PersonFileLine> itemsInformationMapper = new BeanWrapperFieldSetMapper<>();
        itemsInformationMapper.setTargetType(PersonFileLine.class);
        return itemsInformationMapper;
    }
}
