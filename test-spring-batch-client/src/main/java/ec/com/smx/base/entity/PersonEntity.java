package ec.com.smx.base.entity;

import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import ec.com.kruger.spring.orm.entity.AbstractBaseAuditableLockingIp;
import ec.com.smx.frameworkv2.security.view.UserView;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Person entity.
 *
 * @author klopez
 * @version 1.0
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "SSPCOTPERSONA")
public class PersonEntity extends AbstractBaseAuditableLockingIp<UserView, Integer> {

    private static final long serialVersionUID = -1309767405453638579L;
    
    @Id
    @GeneratedValue(generator = "SEQUENCE_PERSON", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "SEQUENCE_PERSON", sequenceName = "SEQUENCE_PERSON")
    @Column(name = "CODIGOPERSONA", nullable = false)
    private Integer codePerson;

    @Column(name = "TIPODOCUMENTO")
    private String identifyType;

    @Column(name = "NUMERODOCUMENTO")
    private String identifyNumber;

    @Column(name = "PRIMERNOMBRE")
    private String firstName;

    @Column(name = "SEGUNDONOMBRE")
    private String lastName;

    @Column(name = "NOMBRECOMPLETO")
    private String fullName;

    @Column(name = "ESTADOPERSONA")
    private String statusString;

    @Column(name = "GENEROPERSONA")
    private String gender;

    @OneToMany(mappedBy = "person")
    private Collection<FunctionaryEntity> functionaries;

    /**
     * Get entity id.
     */
    @Override
    public Integer getId() {
        return codePerson;
    }
}
