package ec.com.smx.base.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import ec.com.smx.base.vo.FindPersonResponseV2;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import ec.com.smx.base.entity.PersonEntity;
import ec.com.smx.base.exception.IdentifyNumberException;
import ec.com.smx.base.repository.PersonRepository;
import ec.com.smx.base.vo.FindPersonRequest;
import ec.com.smx.base.vo.FindPersonResponse;

/**
 * Test for PersonService class.
 *
 * @author jlage on 01/03/2019.
 * @version 1.0
 * @since 1.0.0
 */

@RunWith(MockitoJUnitRunner.Silent.class)
public class PersonServiceTest {

    private static final String INVALID_IDENTIFY_NUMBER = "123456789";
    private static final String VALID_IDENTIFY_NUMBER = "1234";
    private static final String FIRST_NAME = "First name";
    private static final String FULL_NAME = "Full name";

    private PersonEntity personEntity;

    @InjectMocks
    private PersonService personService;

    @Mock
    private PersonRepository personRepository;

    @Before
    public void setup() {
        // En caso de no acceda la inyeccion por multiples repositories.
        // MockitoAnnotations.initMocks(this);
        personEntity = PersonEntity.builder().identifyNumber(INVALID_IDENTIFY_NUMBER).build();

        List<PersonEntity> personEntityList = new ArrayList<>();
        personEntityList.add(PersonEntity.builder().firstName(FIRST_NAME).build());

        when(personRepository.findByIdentifyNumber(INVALID_IDENTIFY_NUMBER))
            .thenReturn(personEntity);

        when(personRepository.findByFirstName(FIRST_NAME)).thenReturn(personEntityList);

        List<FindPersonResponse> findPersonResponseList = new ArrayList<>();
        findPersonResponseList.add(FindPersonResponse.builder().firstName(FIRST_NAME).build());

        when(
            personRepository.findPersons(FindPersonRequest.builder().firstName(FIRST_NAME).build()))
            .thenReturn(findPersonResponseList);
    }

    @Test(expected = IdentifyNumberException.class)
    public void testSaveWithValidationThrowException() {
        personService.saveWithValidation(personEntity);
    }

    @Test
    public void testSaveWithValidationVerifyNotSave() {
        try {
            personService.saveWithValidation(personEntity);
        } catch (IdentifyNumberException e) {
            verify(personRepository, times(1)).findByIdentifyNumber(INVALID_IDENTIFY_NUMBER);
            verify(personRepository, times(0)).save(personEntity);
        }
    }

    @Test
    public void testSaveWithValidationNotThrowException() {
        PersonEntity expectedPersonEntity = PersonEntity.builder()
            .identifyNumber(VALID_IDENTIFY_NUMBER).build();
        personService.saveWithValidation(expectedPersonEntity);
        verify(personRepository, times(1)).findByIdentifyNumber(VALID_IDENTIFY_NUMBER);
        verify(personRepository, times(1)).save(expectedPersonEntity);
    }

    @Test
    public void testFindByFirstNameIsNotEmpty() {
        PersonEntity expectedPersonEntity = PersonEntity.builder().firstName(FIRST_NAME).build();
        List<PersonEntity> personEntityList = personService.findByFirstName(FIRST_NAME);
        PersonEntity personEntity = personEntityList.get(0);
        // Assert.assertEquals("First name are not equals",
        // expectedPersonEntity.getFirstName(), personEntity.getFirstName());
    }

    @Test
    public void testFindPersonsIsNotEmpty() {
        FindPersonRequest findPersonRequest = FindPersonRequest.builder().firstName(FIRST_NAME)
            .build();
        FindPersonResponse expectedFindPersonResponse = FindPersonResponse.builder()
            .firstName(FIRST_NAME).build();
        List<FindPersonResponse> findPersonResponseList = personService
            .findPersons(findPersonRequest);
        FindPersonResponse findPersonResponse = findPersonResponseList.get(0);
        Assert.assertEquals("First name are not equals", expectedFindPersonResponse.getFirstName(),
            findPersonResponse.getFirstName());
    }

    @Test
    public void testFindSimplePersonsIsNotEmpty() {
        //Arrange
        FindPersonRequest findPersonRequest = FindPersonRequest.builder().fullName(FULL_NAME)
            .build();
        List<FindPersonResponseV2> findPersonResponseList = new ArrayList<>();
        findPersonResponseList.add(FindPersonResponseV2.builder().fullName(FULL_NAME).build());
        when(personRepository.findSimplePersons(findPersonRequest))
            .thenReturn(findPersonResponseList);

        //Act
        List<FindPersonResponseV2> personEntityList = this.personService
            .findSimplePersons(findPersonRequest);
        FindPersonResponseV2 findPersonResponseV2 = personEntityList.get(0);

        //Assert
        Assert.assertEquals("First name are not equals",
            findPersonResponseV2.getFullName(), FULL_NAME);
    }
}
