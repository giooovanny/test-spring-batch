package ec.com.smx.base.entity;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import org.junit.Test;

import com.github.javafaker.Faker;

/**
 * Test id access functionary entity.
 *
 * @version 1.0
 * @autor klopez on 3/1/19.
 * @since 1.0.0
 */
public class FunctionaryEntityTest {

    private Faker faker = new Faker();

    @Test
    public void testGetId() {
        FunctionaryEntity func = FunctionaryEntity.builder().functionaryCode(faker.code().asin())
            .build();

        assertThat(func.getFunctionaryCode(), is(func.getId()));
    }

}
