package ec.com.smx.base.repository;

import static com.querydsl.core.types.Projections.bean;
import static ec.com.smx.base.entity.QPersonEntity.personEntity;
import java.util.Collection;
import java.util.List;
import ec.com.smx.base.vo.FindPersonResponseV2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.JPQLQuery;
import ec.com.kruger.spring.orm.dto.SearchModelDTO;
import ec.com.kruger.spring.orm.jpa.repository.JPAQueryDslBaseRepository;
import ec.com.smx.base.entity.PersonEntity;
import ec.com.smx.base.vo.FindPersonRequest;
import ec.com.smx.base.vo.FindPersonResponse;

/**
 * Person repository compile with JPA and QUERYDSL.
 *
 * @author klopez on 2/20/19.
 * @version 1.0
 * @since 1.0.0
 */
@Lazy
@Repository
public class PersonRepository extends JPAQueryDslBaseRepository<PersonEntity> implements
    IPersonRepository {

    private static final String STATUS_ACTIVE = "ACT";

    /**
     * Constructor.
     */
    public PersonRepository() {
        super(PersonEntity.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PersonEntity> findByFirstName(String firstName) {
        return from(personEntity)
            .distinct()
            .where(personEntity.statusString.eq(STATUS_ACTIVE),
                personEntity.firstName.contains(firstName))
            .fetch();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FindPersonResponse> findPersons(FindPersonRequest personRequest) {
        final BooleanExpression booleanExpression = this.getRestrictionByFilter(personRequest);

        return from(personEntity)
            .select(bean(FindPersonResponse.class,
                personEntity.codePerson,
                personEntity.firstName,
                personEntity.lastName,
                personEntity.fullName))
            .where(booleanExpression)
            .fetch();
    }

    /**
     * Method to get restrictions by filter.
     *
     * @param personRequest Request structure.
     * @return BooleanExpression expression.
     */
    private BooleanExpression getRestrictionByFilter(FindPersonRequest personRequest){
        BooleanExpression where = personEntity.statusString.eq(STATUS_ACTIVE);

        if (personRequest.getCodePerson() != null) {
            where = where.and(personEntity.codePerson.eq(personRequest.getCodePerson()));
        }

        if (StringUtils.isNotEmpty(personRequest.getFullName())) {
            where = where
                .and(personEntity.fullName.containsIgnoreCase(personRequest.getFullName()));
        }

        if (StringUtils.isNotEmpty(personRequest.getFirstName())) {
            where = where
                .and(personEntity.firstName.containsIgnoreCase(personRequest.getFirstName()));
        }

        if (StringUtils.isNotEmpty(personRequest.getLastName())) {
            where = where
                .and(personEntity.lastName.containsIgnoreCase(personRequest.getLastName()));
        }
        return where;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public PersonEntity findByIdentifyNumber(final String identityNumber) {
        return from(personEntity)
            .where(personEntity.statusString.eq(STATUS_ACTIVE),
                personEntity.identifyNumber.eq(identityNumber))
            .fetchFirst();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Page<PersonEntity> findPagedData(Pageable pageable) {
        JPQLQuery<PersonEntity> query = from(personEntity)
            .where(personEntity.statusString.eq(STATUS_ACTIVE));
        return findPagedData(query, pageable);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Page<PersonEntity> findPagedDataWithGenericFilters(
        Collection<SearchModelDTO<?>> searchModelDTOs,
        Pageable pageable) {
        JPQLQuery<PersonEntity> query = from(personEntity)
            .where(personEntity.statusString.eq(STATUS_ACTIVE));
        return findPagedData(query, pageable, searchModelDTOs);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FindPersonResponseV2> findSimplePersons(FindPersonRequest personRequest) {
        final BooleanExpression booleanExpression = this.getRestrictionByFilter(personRequest);
        return from(personEntity)
            .select(bean(FindPersonResponseV2.class,
                personEntity.codePerson,
                personEntity.fullName))
            .where(booleanExpression)
            .fetch();
    }
}
