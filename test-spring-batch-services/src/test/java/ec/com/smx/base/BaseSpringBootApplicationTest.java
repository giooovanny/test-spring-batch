package ec.com.smx.base;

import ec.com.smx.base.config.BaseConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test main spring boot app.
 *
 * @version 1.0
 * @autor scuascota on 27/8/20.
 * @since 1.0.0
 */
@RunWith(SpringRunner.class)
@Import(BaseConfiguration.class)
@SpringBootTest(classes = {BaseSpringBootApplication.class, BaseConfiguration.class})
public class BaseSpringBootApplicationTest {

    @Autowired
    private PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer;

    @Test
    public void testApplicationContextLoaded() {
        assertThat(propertySourcesPlaceholderConfigurer).isNotNull();
    }

    @Test
    public void testApplicationStarts() {
        BaseSpringBootApplication.main("--spring.main.web-environment=true",
            " -Dspring-boot.run.jvmArguments=\"-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5005\"");
        assertThat(propertySourcesPlaceholderConfigurer).isNotNull();
    }

    @Test
    public void testApplicationStartsNull() {
        BaseSpringBootApplication.main("--spring.main.web-envirent=false");
        assertThat(propertySourcesPlaceholderConfigurer).isNotNull();
    }

}
