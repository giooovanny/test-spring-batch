package ec.com.smx.base.batch.job;

import ec.com.smx.base.batch.listener.JobListener;
import ec.com.smx.base.common.BaseConstants;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

/**
 * Batch jobs .
 *
 * @author gcadena on 23/3/20.
 * @version 1.0
 * @since 1.
 */
@Lazy
@Configuration
@EnableBatchProcessing
public class BaseBatchJobs {

    /**
     * Migrates exchange files.
     *
     * @param loadExchangeStep Step for execution.
     * @param baseJR Job repository.
     * @return Job.
     */
    @Bean
    @Lazy
    @Autowired
    public Job exchangeFilesJob(JobBuilderFactory jobs,
                                @Qualifier("loadExchangeStep") Step loadExchangeStep,
                                @Qualifier("baseJR") JobRepository baseJR, JobListener listener) {
        return jobs.get(BaseConstants.JOB_EXCHANGE_FILES)
                .incrementer(new RunIdIncrementer())
                .repository(baseJR)
                .start(loadExchangeStep)
                .listener(listener)
                .build();
    }

}
