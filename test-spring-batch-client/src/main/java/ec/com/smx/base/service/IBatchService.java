package ec.com.smx.base.service;

/**
 * Batch services specification.
 *
 * @author gcadena on 23/3/20.
 * @version 1.0
 * @since 1.
 */
public interface IBatchService {

    /**
     * Execute Job.
     *
     */
    void executeJob();
}
