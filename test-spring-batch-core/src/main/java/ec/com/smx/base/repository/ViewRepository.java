package ec.com.smx.base.repository;

import static ec.com.smx.base.entity.QUserViewSample.userViewSample;
import java.util.List;
import ec.com.kruger.spring.orm.jpa.repository.JPAQueryDslBaseRepository;
import ec.com.smx.base.entity.UserViewSample;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

/**
 * UserView repository compile with JPA and QUERYDSL.
 */
@Lazy
@Repository
public class ViewRepository extends JPAQueryDslBaseRepository<UserViewSample> implements IViewRepository {

    /**
     * Constructor.
     */
    public ViewRepository() {
        super(UserViewSample.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UserViewSample> findByFirstName(String firstName) {
        return from(userViewSample)
            .distinct()
            .where(userViewSample.usercomnom.eq(firstName))
            .fetch();
    }
}
