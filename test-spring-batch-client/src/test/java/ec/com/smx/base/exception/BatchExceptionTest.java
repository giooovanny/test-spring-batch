package ec.com.smx.base.exception;

import org.junit.Assert;
import org.junit.Test;

/**
 * Test for custom exception BatchException.
 *
 * @author gcadena on 23/3/20.
 * @version 1.0
 * @since 1.
 */
public class BatchExceptionTest {

    @Test
    public void testThrowExceptionWithValidMessage() {
        try {
            throw new BatchException("Test custom message");
        } catch (BatchException e) {
            Assert.assertEquals("Messages are not equal", "Test custom message", e.getMessage());
        }
    }
}
