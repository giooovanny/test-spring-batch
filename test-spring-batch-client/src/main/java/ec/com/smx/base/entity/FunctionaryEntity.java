package ec.com.smx.base.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import ec.com.kruger.spring.orm.entity.AbstractBaseAuditableLockingIp;
import ec.com.smx.frameworkv2.security.view.UserView;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Functionary entity.
 *
 * @author klopez
 * @version 1.0
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "SSPCOTFUNCIONARIO")
public class FunctionaryEntity extends AbstractBaseAuditableLockingIp<UserView, String> {

    private static final long serialVersionUID = -6514260485577097572L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CODIGOFUNCIONARIO")
    private String functionaryCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODIGOPERSONA", referencedColumnName = "CODIGOPERSONA", insertable = false, updatable = false)
    private PersonEntity person;

    /**
     * Get entity id.
     */
    @Override
    public String getId() {
        return functionaryCode;
    }
}
