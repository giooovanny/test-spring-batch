package ec.com.smx.base.vo;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Class VO for get response of find person process.
 *
 * @version 1.0
 * @autor klopez
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FindPersonResponse {

    @NotNull
    private Integer codePerson;
    private String firstName;
    private String lastName;
    private String fullName;
}
