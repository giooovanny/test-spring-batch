package ec.com.smx.base.service;

import ec.com.smx.base.entity.PersonEntityTest;
import ec.com.smx.base.repository.IPersonTestRepository;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import ec.com.kruger.spring.service.jpa.BaseService;

/**
 * Person service implementation.
 *
 * @author gcadena on 26/03/21.
 * @version 1.0
 * @since 1.0.0
 */
@Transactional("jpaTransactionManager")
@Validated
@Lazy
@Service
public class PersonTestService extends BaseService<PersonEntityTest, IPersonTestRepository> implements
        IPersonTestService {

    /**
     * Constructor with dependencies.
     *
     * @param repository The repository to inject
     */
    public PersonTestService(IPersonTestRepository repository) {
        super(repository);
    }

    /**
     * Save persons test.
     *
     * @param personEntityTest new Person test
     * @return A person test object
     */
    @Override
    @Transactional(value = "jpaTransactionManager")
    public void savePerson(PersonEntityTest personEntityTest) {
        repository.savePerson(personEntityTest);
    }

}
