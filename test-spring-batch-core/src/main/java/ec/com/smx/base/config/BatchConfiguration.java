package ec.com.smx.base.config;

import javax.sql.DataSource;

import ec.com.smx.base.common.BaseConstants;

import ec.com.smx.base.exception.BatchException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.configuration.annotation.*;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.MapJobRepositoryFactoryBean;
import org.springframework.batch.support.transaction.ResourcelessTransactionManager;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.*;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * Spring batch configuration.
 *
 * @author gcadena on 19/3/20.
 * @version 1.0
 * @since 1.
 */
@EnableBatchProcessing
@Configuration
@EnableAutoConfiguration
@ComponentScan("ec.com.smx.base.batch.*")
@Slf4j
public class BatchConfiguration extends DefaultBatchConfigurer {

    private static final int CORE_POOL_SIZE = 1;
    private static final String ISOLATION_DEFAULT = "ISOLATION_DEFAULT";

    /**
     * {@inheritDoc}
     */
    @Override
    public void setDataSource(DataSource dataSource) {
        log.info("---CONFIG-LOG: Setting base spring batch data source");
    }

    /**
     * Resource less TransactionManager.
     *
     * @return TransactionManager.
     */
    @Bean
    @Lazy
    public PlatformTransactionManager baseResourceLessTM() {
        return new ResourcelessTransactionManager();
    }

    /**
     * Job repository.
     */
    @Bean
    protected JobRepository baseJR() {
        try {
            MapJobRepositoryFactoryBean factory = new MapJobRepositoryFactoryBean(
                    baseResourceLessTM());
            factory.setIsolationLevelForCreate(ISOLATION_DEFAULT);
            factory.setValidateTransactionState(false);
            factory.afterPropertiesSet();
            return factory.getObject();
        } catch (Exception e) {
            throw new BatchException("Batch Error. ", e);
        }
    }

    /**
     * Job Launcher.
     */
    @Lazy
    @Bean(BaseConstants.JOB_LAUNCHER)
    protected SimpleJobLauncher baseJobLauncher() {
        try {
            SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
            jobLauncher.setJobRepository(baseJR());
            jobLauncher.afterPropertiesSet();
            return jobLauncher;
        } catch (Exception e) {
            throw new BatchException("Batch Error. ", e);
        }
    }

    /**
     * Task executor.
     */
    @Bean
    protected ThreadPoolTaskExecutor baseTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(CORE_POOL_SIZE);
        return executor;
    }

    /**
     * Clase listener para job.
     */
    @Component
    public class JobCompletionNotificationListener extends JobExecutionListenerSupport {

        /**
         * {@inheritDoc}
         */
        @Override
        public void afterJob(JobExecution jobExecution) {
            if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
                log.info("---BATCH-LOG: JOB FINISHED! Results: ");
            }
        }
    }
}
