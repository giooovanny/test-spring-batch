package ec.com.smx.base.repository;

import ec.com.kruger.spring.orm.dto.SearchModelDTO;
import ec.com.smx.base.entity.PersonEntity;
import ec.com.smx.base.vo.FindPersonRequest;
import ec.com.smx.base.vo.FindPersonResponse;
import ec.com.smx.base.vo.FindPersonResponseV2;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Test for PersonRepository class.
 *
 * @author jlage on 26/02/2019.
 * @version 1.0
 * @since 1.0.0
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class PersonRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private IPersonRepository repository;

    @Before
    public void setup() {
        entityManager.persist(PersonEntity.builder()
            .firstName("First Name")
            .lastName("Last Name")
            .fullName("First Last Name")
            .build());
        entityManager.persist(PersonEntity.builder()
            .firstName("First Null Name")
            .fullName("Full Null Name")
            .statusString("ACT")
            .build());
        entityManager.persist(PersonEntity.builder()
            .lastName("Last Null Name")
            .fullName("Full Null Name")
            .statusString("ACT").build());
        entityManager.persist(PersonEntity.builder()
            .firstName("First Code")
            .lastName("Last Code")
            .fullName("First Last Code")
            .build());
        entityManager.persist(PersonEntity.builder()
            .identifyNumber("1752387504")
            .firstName("First Name")
            .lastName("Last Name")
            .fullName("First Last Name")
            .build());
    }

    @Test
    public void testFindByFirstNameEmptyList() {
        List<PersonEntity> personEntityList = repository.findByFirstName("Never Appear This Name");
        Assert.assertTrue("Empty collection", personEntityList.isEmpty());
    }

    @Test
    public void testFindByFirstName() {
        PersonEntity expectedPersonEntity = PersonEntity.builder().firstName("Kelly")
            .lastName("Lopez")
            .fullName("Kelly Lopez").build();

        List<PersonEntity> personEntityList = repository.findByFirstName("Kelly");
        PersonEntity personEntity = personEntityList.get(0);
        Assert.assertEquals("No expected first name", personEntity.getFirstName(),
            expectedPersonEntity.getFirstName());
    }

    @Test
    public void testFindPersonsWithCodePersonIsNotNull() {
        List<PersonEntity> personEntityList = repository.findByFirstName("Kenny");
        PersonEntity personEntity = personEntityList.get(0);
        FindPersonRequest expectedPersonRequest = FindPersonRequest.builder()
            .codePerson(personEntity.getCodePerson()).build();
        List<FindPersonResponse> personResponseList = repository.findPersons(expectedPersonRequest);
        FindPersonResponse findPersonResponse = personResponseList.get(0);
        Assert.assertEquals("No expected code person", expectedPersonRequest.getCodePerson(),
            findPersonResponse.getCodePerson());
    }

    @Test
    public void testFindPersonsWithFullNameIsNotEmpty() {
        FindPersonRequest expectedPersonRequest = FindPersonRequest.builder()
            .fullName("Kenny Lopez").build();
        List<FindPersonResponse> personResponseList = repository.findPersons(expectedPersonRequest);
        FindPersonResponse findPersonResponse = personResponseList.get(0);
        Assert.assertEquals("No expected full name", expectedPersonRequest.getFullName(),
            findPersonResponse.getFullName());
    }

    @Test
    public void testFindPersonsWithFirstNameIsNotEmpty() {
        FindPersonRequest expectedPersonRequest = FindPersonRequest.builder()
            .firstName("First Null Name").build();
        List<FindPersonResponse> personResponseList = repository.findPersons(expectedPersonRequest);
        FindPersonResponse findPersonResponse = personResponseList.get(0);
        Assert.assertEquals("No expected first name", expectedPersonRequest.getFirstName(),
            findPersonResponse.getFirstName());
    }

    @Test
    public void testFindPersonsWithLastNameIsNotEmpty() {
        FindPersonRequest expectedPersonRequest = FindPersonRequest.builder()
            .lastName("Last Null Name").build();
        List<FindPersonResponse> personResponseList = repository.findPersons(expectedPersonRequest);
        FindPersonResponse findPersonResponse = personResponseList.get(0);
        Assert.assertEquals("No expected last name", expectedPersonRequest.getLastName(),
            findPersonResponse.getLastName());
    }

    @Test
    public void testFindByIdentifyNumberIsNotNull() {
        PersonEntity personEntity = repository.findByIdentifyNumber("1756880249");
        Assert.assertNotNull("Invalid identify number", personEntity);
    }

    @Test
    public void testFindPaginated() {
        Page<PersonEntity> result = repository.findPagedData(PageRequest.of(2, 2));

        Assert.assertNotNull("Not null page", result);
        Assert.assertEquals("Check total result", 5, result.getTotalElements());
        Assert.assertEquals("Check total pages", 3, result.getTotalPages());
        Assert.assertEquals("Check elements of current page", 1, result.getNumberOfElements());
    }

    @Test
    public void testFindPaginatedWithGenericFilterEqual() {
        int codePerson = 1;
        final SearchModelDTO<Integer> filter1 = SearchModelDTO.<Integer>builder()
            .dataType("integer").parameterPattern("codePerson").parameterValue(codePerson)
            .comparatorTypeEnum("Igual").selectedOption("uv").operatorOR(false).insensitive(false)
            .rangeBean(null).build();

        Page<PersonEntity> result = repository
            .findPagedDataWithGenericFilters(Arrays.asList(filter1), PageRequest.of(0, 2));

        Assert.assertNotNull("Not null page", result);
        Assert.assertEquals("Check total result", 1, result.getTotalElements());
        Assert.assertEquals("Check total pages", 1, result.getTotalPages());
        Assert.assertEquals("Check elements of current page", 1, result.getNumberOfElements());

        assertThat(result.getContent().get(0).getId(), is(codePerson));
    }

    @Test
    public void testFindPaginatedWithGenericFilterIn() {
        int codePerson1 = 1;
        int codePerson2 = 2;

        final SearchModelDTO<Integer> filter2 = SearchModelDTO.<Integer>builder()
            .dataType("integer").parameterPattern("codePerson")
            .parameterValues(Arrays.asList(codePerson1, codePerson2))
            .comparatorTypeEnum("Igual").selectedOption("lv").operatorOR(false).insensitive(false)
            .rangeBean(null).build();

        Page<PersonEntity> result = repository
            .findPagedDataWithGenericFilters(Arrays.asList(filter2), PageRequest.of(0, 2));

        Assert.assertNotNull("Not null page", result);
        Assert.assertEquals("Check total result", 2, result.getTotalElements());
        Assert.assertEquals("Check total pages", 1, result.getTotalPages());
        Assert.assertEquals("Check elements of current page", 2, result.getNumberOfElements());

        assertThat(result.getContent().get(0).getId(), is(codePerson1));
        assertThat(result.getContent().get(1).getId(), is(codePerson2));
    }

    @Test
    public void testFindPaginatedWithGenericFilterInCollection() {
        String functionaryCode1 = "1";

        final SearchModelDTO<String> filter2 = SearchModelDTO.<String>builder()
            .dataType("string").parameterPattern("functionaries.functionaryCode")
            .parameterValues(Arrays.asList(functionaryCode1))
            .comparatorTypeEnum("Igual").selectedOption("lv").operatorOR(false).insensitive(false)
            .rangeBean(null).build();

        Page<PersonEntity> result = repository
            .findPagedDataWithGenericFilters(Arrays.asList(filter2), PageRequest.of(0, 2));

        Assert.assertNotNull("Not null page", result);
        Assert.assertEquals("Check total result", 0, result.getTotalElements());
        Assert.assertEquals("Check total pages", 0, result.getTotalPages());
        Assert.assertEquals("Check elements of current page", 0, result.getNumberOfElements());
    }

    @Test
    public void testFindSimplePersonsWithFirstNameIsNotEmpty() {
        //Arrange
        FindPersonRequest expectedPersonRequest = FindPersonRequest.builder()
            .fullName("Full Null Name").build();

        //Act
        List<FindPersonResponseV2> personResponseList = repository.findSimplePersons(expectedPersonRequest);
        FindPersonResponseV2 findPersonResponse = personResponseList.get(0);

        //Assert
        Assert.assertEquals("No expected first name", expectedPersonRequest.getFullName(),
            findPersonResponse.getFullName());
    }
}
