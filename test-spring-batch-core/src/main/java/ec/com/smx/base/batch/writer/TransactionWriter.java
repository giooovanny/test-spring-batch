package ec.com.smx.base.batch.writer;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Random;

import ec.com.smx.base.entity.PersonEntityTest;
import ec.com.smx.base.service.IPersonTestService;
import ec.com.smx.base.vo.batch.PersonFileLine;
import lombok.Getter;
import lombok.Setter;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;

/**
 * Writes results of migration file processing in database.
 *
 * @author gcadena on 23/3/20.
 * @version 1.0
 * @since 1.
 */
@Lazy
@Component
@StepScope
@Getter
@Setter
public class TransactionWriter implements ItemWriter<PersonFileLine> {

    @Lazy
    @Autowired
    @Getter
    private IPersonTestService personTestService;

//    FileSystemResource fsr;
//    /**
//     * constructor.
//     */
//    public TransactionWriter(@Value("#{jobParameters['file.output']}") String fileOutput) throws IOException {
//        fsr = new FileSystemResource(new File(fileOutput));
//    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void write(List<? extends PersonFileLine> fileLines) throws IOException {
        Random rm = new Random();
        for (PersonFileLine fileLine : fileLines) {
            PersonEntityTest person = new PersonEntityTest(823023+rm.nextInt(8453457),fileLine.getIdentifyType(),
                    fileLine.getIdentifyNumber(),fileLine.getFirstName(),fileLine.getLastName(),
                    fileLine.getFullName(),fileLine.getStatusString(),fileLine.getGender());
            personTestService.savePerson(person);

//            fsr.getOutputStream().write(person.toString().getBytes());
        }
    }

}
