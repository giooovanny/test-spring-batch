package ec.com.smx.base.vo;

import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Class VO for get response of find person v2 process.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FindPersonResponseV2 {

    @NotBlank
    private Integer codePerson;

    private String fullName;

}
