package ec.com.smx.base.batch.processor;

import ec.com.smx.base.vo.batch.PersonFileLine;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;

/**
 * Person item process.
 *
 * @author gcadena on 19/3/20.
 * @version 1.0
 * @since 1.
 */
@Slf4j
public class PersonItemProcessor implements ItemProcessor<PersonFileLine, PersonFileLine> {

    @Override
    public PersonFileLine process(PersonFileLine item) throws Exception {
        item.setFirstName(item.getFirstName().toUpperCase());
        item.setLastName(item.getLastName().toUpperCase());
        item.setFullName(item.getFirstName().toUpperCase()+item.getLastName().toUpperCase());

        log.info("Dato Ejecutado en BATCH: ( "+item.getFullName()+" )");
        return item;
    }
}