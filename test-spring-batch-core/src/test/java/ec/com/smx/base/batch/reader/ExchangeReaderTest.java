package ec.com.smx.base.batch.reader;

import ec.com.smx.base.config.BaseConfiguration;
import ec.com.smx.base.config.BatchConfiguration;
import ec.com.smx.base.vo.batch.PersonFileLine;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.test.MetaDataInstanceFactory;
import org.springframework.batch.test.StepScopeTestUtils;
import org.springframework.batch.test.context.SpringBatchTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Test for ExchangeReader class.
 *
 * @author gcadena on 26/03/21.
 * @version 1.0
 * @since 1.0.0
 */
@SpringBatchTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {BaseConfiguration.class, BatchConfiguration.class})
public class ExchangeReaderTest {

    @Autowired
    FlatFileItemReader<PersonFileLine> itemReader;

    @Test
    public void testStepWhenReaderCalledThenSuccess() throws Exception {
        // ARRANGE
        JobParametersBuilder executionParams = new JobParametersBuilder();
        executionParams.addString("file.input", "batch/dat.csv");
        StepExecution stepExecution = MetaDataInstanceFactory
                .createStepExecution(executionParams.toJobParameters());

        // ACT
        StepScopeTestUtils.doInStepScope(stepExecution, () -> {
            PersonFileLine personFileLine;
            itemReader.open(stepExecution.getExecutionContext());
            while ((personFileLine = itemReader.read()) != null) {

                // ASSERT
                Assert.assertEquals("uno",personFileLine.getIdentifyType());
                Assert.assertEquals("11111",personFileLine.getIdentifyNumber());
                Assert.assertEquals("giovanny",personFileLine.getFirstName());
                Assert.assertEquals("cadena",personFileLine.getLastName());
                Assert.assertEquals("giovannycadena",personFileLine.getFullName());

            }
            itemReader.close();
            return null;
        });
    }
}
