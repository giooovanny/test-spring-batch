package ec.com.smx.base.entity;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import org.junit.Test;

import com.github.javafaker.Faker;

/**
 * Test id access person entity.
 *
 * @version 1.0
 * @autor klopez on 3/1/19.
 * @since 1.0.0
 */
public class PersonEntityTest {

    private Faker faker = new Faker();

    @Test
    public void testGetId() {
        PersonEntity person = PersonEntity.builder().codePerson(faker.number().randomDigitNotZero())
            .firstName(faker.name().firstName())
            .lastName(faker.name().lastName()).fullName(faker.name().fullName()).build();

        assertThat(person.getCodePerson(), is(person.getId()));
    }

}
