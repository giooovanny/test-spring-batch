INSERT INTO kssegtuser(user_id, usercomnom, useremail,usernom, userpasswordcaducitydate, userpasswordencrypted, userpwd,userstatus)
VALUES('FRM0','KENNY LOPEZ','KN@','KENNY', NULL,NULL,'PASSWORD01','ACT');

INSERT INTO sspcotpersona(codigopersona, company_code, created_by_user,created_from_ip, status, version, tipodocumento, numerodocumento, primernombre, segundonombre, nombrecompleto, estadopersona, generopersona)
VALUES (1, 1, 'FRM0', '127.0.0.1', 1, 1, 'CI', '1756880249', 'Kenny', 'Lopez', 'Kenny Lopez', 'ACT', 'MAS'),
       (2, 1, 'FRM0', '127.0.0.1', 1, 1, 'CI', '1757120421', 'Maylin', 'Diaz', 'Maylin Diaz', 'ACT', 'FEM'),
       (3, 1, 'FRM0', '127.0.0.1', 1, 1, 'CI', '1757120423', 'Kelly', 'Lopez', 'Kelly Lopez', 'ACT', 'FEM');

-- En caso de utilizar una vista se puede gestionar como entidad.
--Opcion A
INSERT INTO SSPCOVUSERS(user_id, usercomnom)
VALUES ('FRM0', 'ejemplo');

-- Opcion B excluyendo de la creacion de entidades con un provider y filter personalizado. application.yml schema_filter_provider
--CREATE OR REPLACE VIEW SSPCOVUSERS AS(
--SELECT USU.USER_ID, USU.usercomnom FROM SSPCOTPERSONA PER
--INNER JOIN KSSEGTUSER USU
--ON PER.CREATEDBYID = USU.USER_ID
--WHERE PER.COMPANYCODE = 1);

--INSERT INTO SSPCOTCATALOGOTIPO (CODIGOCATALOGOTIPO, NOMBRECATALOGOTIPO, DESCRIPCIONCATALOGOTIPO, REQUIEREVALORNUMERICO, REQUIEREORDEN, ESTADO, CODIGOCATALOGOTIPOPADRE, FECHAREGISTRO, USUARIOREGISTRO, FECHAMODIFICACION, USUARIOMODIFICACION) VALUES(100600, 'PLATAFORMA DE MENSAJERIA', 'LISTADO DE PLATAFORMAS DE MENSAJERIA PUBLICITARIA', '0', '0', 'ACT', NULL, NULL, NULL, NULL, NULL);
--INSERT INTO SSPCOTCATALOGOVALOR (CODIGOCATALOGOVALOR, CODIGOCATALOGOTIPO, NOMBRECATALOGOVALOR) VALUES('SMS', 100600, 'Sms');
--INSERT INTO SSPCOTCATALOGOVALOR (CODIGOCATALOGOVALOR, CODIGOCATALOGOTIPO, NOMBRECATALOGOVALOR) VALUES('WHA', 100600, 'Whatsapp');
