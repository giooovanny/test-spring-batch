package ec.com.smx.base;

import java.util.Arrays;
import java.util.Objects;
import ec.com.kruger.security.sso.springboot2.configuration.SecurityKeycloakConfiguration;
import ec.com.smx.base.config.BatchConfiguration;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import ec.com.kruger.spring.metric.config.DefaultApplicationContextMetricConfig;
import ec.com.kruger.spring.ws.config.WebConfig;
import ec.com.smx.base.config.BaseConfiguration;
import ec.com.smx.base.entity.PersonEntity;
import ec.com.smx.frameworkv2.security.view.UserView;
import lombok.extern.slf4j.Slf4j;

/**
 * Main class to run spring boot app.
 *
 * @author klopez on 2/21/19.
 * @version 1.0
 * @since 1.0.0
 */
@SpringBootApplication(scanBasePackages = "ec.com.smx.base")
@Import({BaseConfiguration.class, BatchConfiguration.class,
    DefaultApplicationContextMetricConfig.class,
    WebConfig.class,
    SecurityKeycloakConfiguration.class})
@EntityScan(basePackageClasses = {PersonEntity.class, UserView.class})
@Slf4j
public class BaseSpringBootApplication extends SpringBootServletInitializer implements
    WebMvcConfigurer {

    @Autowired
    private BuildProperties buildProperties;

    /**
     * Main to run app.
     *
     * @param args args to pass app
     */
    public static void main(String... args) {
        try {
            SpringApplication app = new SpringApplication(BaseSpringBootApplication.class);
            app.run(args);
        } catch (Exception throwable) {
            if (!Objects.equals(throwable.getClass().getName(),
                "org.springframework.boot.devtools.restart.SilentExitExceptionHandler$SilentExitException")
                && log.isErrorEnabled()) {
                log.error(
                    "********************************Ha ocurrido una exception****************************");
                log.error("Exception: " + throwable.toString());
                log.error("Root Cause: " + ExceptionUtils.getRootCause(throwable).toString());
            }
        }
    }

    /**
     * commandLineRunner.
     *
     * @param ctx the ctx
     * @return CommandLineRunner instance
     */
    @Bean
    public CommandLineRunner commandLineRunner(ListableBeanFactory ctx) {
        if (log.isDebugEnabled()) {
            log.debug("Beans Loaded by Spring Boot:{}", ctx.getBeanDefinitionCount());
        }
        return args -> {
            if (log.isDebugEnabled()) {
                String[] beanNames = ctx.getBeanDefinitionNames();
                Arrays.sort(beanNames);
                for (String beanName : beanNames) {
                    log.debug("Bean:{}", beanName);
                }
            }
        };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**").allowedOrigins("*")
            .allowedMethods("GET", "POST", "PUT", "DELETE");
    }

    /**
     * Open api bean definition.
     *
     * @return
     */
    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI()
            .security(Arrays.asList(new SecurityRequirement().addList("bearerAuth")))
            .components(new Components().addSecuritySchemes("bearerAuth",
                new SecurityScheme().type(SecurityScheme.Type.HTTP).scheme("bearer")
                    .bearerFormat("JWT")))
            .info(new Info().title("REST API Documentation")
                .description("REST API Documentation for services")
                .version(buildProperties.getVersion())
                .contact(new Contact()
                    .name("")
                    .email("admin@ec.krugercorporation.com")
                    .url("https://www.krugercorp.com"))
                .license(new License()
                    .name("Apache 2.0")
                    .url("http://www.apache.org/licenses/LICENSE-2.0.html")))
            ;
    }

    /**
     * Grouped Bean to all APIs with `/v1/person` in the path.
     *
     * @return GroupedOpenApi
     */
    @Bean
    public GroupedOpenApi accountsV1Apis() {
        return GroupedOpenApi.builder().group("personsV1").pathsToMatch("/**/v1/person/**").build();
    }

    /**
     * Grouped Bean to all APIs with `/v2/person` in the path.
     *
     * @return GroupedOpenApi
     */
    @Bean
    public GroupedOpenApi accountsV2Apis() { // group all APIs with `admin` in the path
        return GroupedOpenApi.builder().group("personsV2").pathsToMatch("/**/v2/person/**").build();
    }

}
