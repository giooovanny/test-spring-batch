package ec.com.smx.base;

import java.util.Objects;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import ec.com.kruger.spring.boot.test.SecurityUserInfoTestConfiguration;
import ec.com.smx.base.config.BaseConfiguration;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Import({BaseConfiguration.class, SecurityUserInfoTestConfiguration.class})
@SpringBootApplication(scanBasePackages = {"ec.com.smx.base"})
public class AppBaseTestSpringBoot {

    /**
     * Main run spring boot app.
     *
     * @param args an array of {@link String} objects.
     */
    public static void main(String... args) {

        try {
            SpringApplication app = new SpringApplication(AppBaseTestSpringBoot.class);
            app.run(args);

        } catch (Exception throwable) {
            if (!Objects.equals(throwable.getClass().getName(),
                "org.springframework.boot.devtools.restart.SilentExitExceptionHandler$SilentExitException")
                && log.isErrorEnabled()) {
                log.error(
                    "*************************************Ha ocurrido una exception**********************************");
                log.error("Exception: " + throwable.toString());
                log.error("Root Cause: " + ExceptionUtils.getRootCause(throwable).toString());
            }
        }

    }
}
