package ec.com.smx.base.config;

import javax.persistence.EntityManagerFactory;

import ec.com.smx.base.entity.PersonEntityTest;
import ec.com.smx.base.repository.PersonTestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ec.com.kruger.spring.orm.jpa.config.SecurityAuditListenerConfig;
import ec.com.smx.base.entity.PersonEntity;
import ec.com.smx.base.repository.PersonRepository;
import ec.com.smx.frameworkv2.security.view.UserView;

/**
 * Base spring configuration.
 *
 * @author klopez on 2/20/19.
 * @version 1.0
 * @since 1.0.0
 */
@EnableJpaRepositories(basePackageClasses = {PersonRepository.class, PersonTestRepository.class})
@EntityScan(basePackageClasses = {PersonEntity.class, PersonEntityTest.class, UserView.class})
@ComponentScan(basePackages = "ec.com.smx.base")
@EnableTransactionManagement
@Import(SecurityAuditListenerConfig.class)
public class BaseConfiguration {

    /**
     * <p>
     * transactionManager.
     * </p>
     *
     * @param emf a {@link javax.persistence.EntityManagerFactory} object.
     * @return a {@link org.springframework.transaction.PlatformTransactionManager} object.
     */
    @Bean
    @Autowired
    @Primary
    public PlatformTransactionManager jpaTransactionManager(EntityManagerFactory emf) {
        JpaTransactionManager txManager = new JpaTransactionManager();
        txManager.setEntityManagerFactory(emf);
        return txManager;
    }



}
