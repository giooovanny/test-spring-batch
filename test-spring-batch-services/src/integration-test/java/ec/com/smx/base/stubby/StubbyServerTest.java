package ec.com.smx.base.stubby;

import static org.hamcrest.core.Is.is;

import java.util.Map;

import org.hamcrest.CoreMatchers;
import org.hamcrest.collection.IsMapContaining;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.cloud.contract.wiremock.WireMockRestServiceServer;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
public class StubbyServerTest {

    protected RestTemplate restTemplate = new RestTemplate();

    protected String wiremockBaseUrl = "http://example.org";

    @Before
    public void init() {

        // will read stubs classpath
        MockRestServiceServer server = WireMockRestServiceServer.with(this.restTemplate)
            .baseUrl(wiremockBaseUrl).stubs("classpath:/stubs/").build();
    }

    @Test
    public void testWireMockJsonService() throws Exception {

        ResponseEntity<Object> response = restTemplate
            .getForEntity(wiremockBaseUrl + "/get", Object.class);

        Assert.assertThat(response.getStatusCodeValue(), CoreMatchers.is(200));
        // Assert.assertThat(response.getBody(),
        // HasPropertyWithValue.hasProperty("message", is("Hello World!")) );
        Assert.assertThat((Map<? extends String, ? extends String>) response.getBody(),
            IsMapContaining.hasEntry("message", "Hello World!"));
    }

    @Test
    public void testWireMockTextPlainService() throws Exception {

        ResponseEntity<String> response = restTemplate
            .getForEntity(wiremockBaseUrl + "/get2", String.class);

        Assert.assertThat(response.getStatusCodeValue(), CoreMatchers.is(200));
        Assert.assertThat(response.getBody(), is("Hello World!"));
    }
}
