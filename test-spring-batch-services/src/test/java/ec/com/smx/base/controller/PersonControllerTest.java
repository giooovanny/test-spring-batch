package ec.com.smx.base.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;

import ec.com.kruger.json.JsonPojoMapperBean;
import ec.com.kruger.spring.ws.controller.test.MockMvcControllerBase;
import ec.com.smx.base.entity.PersonEntity;
import ec.com.smx.base.service.PersonService;
import ec.com.smx.base.vo.FindPersonRequest;
import ec.com.smx.base.vo.FindPersonResponse;

/**
 * Tests para servicios rest y generacion de documentacion de {@link PersonController} usando {@link
 * MockMvc}.
 *
 * @author klopez
 * @version 1.0
 * @since 1.0.0
 */

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(PersonController.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PersonControllerTest extends MockMvcControllerBase {

    @InjectMocks
    private PersonController personController;

    @Mock
    private PersonService personService;

    @Mock
    private ObjectMapper objectMapperMock;

    private ObjectMapper objectMapper = JsonPojoMapperBean.getInstance();

    private Faker faker = new Faker();

    private String firstNameMock = faker.name().firstName();
    private String lastNameMock = faker.name().lastName();

    private FindPersonRequest personRequest = FindPersonRequest.builder().lastName(lastNameMock)
        .build();

    private PersonEntity personEntityResponse1 = PersonEntity.builder().codePerson(1)
        .firstName(firstNameMock).lastName(lastNameMock).build();

    private FindPersonResponse personResponse1 = FindPersonResponse.builder().codePerson(1)
        .firstName(firstNameMock).lastName(lastNameMock).build();
    private FindPersonResponse personResponse2 = FindPersonResponse.builder().codePerson(2)
        .firstName(faker.name().firstName())
        .lastName(faker.name().lastName()).build();
    private FindPersonResponse personResponse3 = FindPersonResponse.builder().codePerson(2)
        .firstName(faker.name().firstName()).lastName(lastNameMock).build();
    private FindPersonResponse personResponse4 = FindPersonResponse.builder().codePerson(2)
        .firstName(faker.name().firstName()).lastName(lastNameMock).build();

    private List<FindPersonResponse> array1 = Arrays
        .asList(personResponse1, personResponse3, personResponse4);

    /**
     * Setup de los mock.
     * <p>
     * Nota: Se llama al setUpBase para inicializar el contexto solo con el controlador que se desea
     * probar.
     * </p>
     *
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     */
    @Before
    public void setup() {
        super.setUpBase(personController);

        given(personService.findByFirstName(firstNameMock))
            .willReturn(Arrays.asList(personEntityResponse1));

        when(personService.findPersons(personRequest)).thenReturn(array1);
    }

    /**
     * Test find person
     *
     * @throws Exception
     */
    @Test
    public void testFindPerson() throws Exception {
        mockMvc
            .perform(post("/baseServices/api/v1/person/findPersons").contextPath("/baseServices")
                .content(objectMapper.writeValueAsString(personRequest))
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())

            .andExpect(jsonPath("$").isArray()).andExpect(jsonPath("$", hasSize(3)))
            .andExpect(jsonPath("$[0].lastName", is(lastNameMock)))
            .andExpect(jsonPath("$[1].lastName", is(lastNameMock)))
            .andExpect(jsonPath("$[2].lastName", is(lastNameMock)))
            .andDo(MockMvcResultHandlers.print());
        verify(personService, times(1)).findPersons(any(FindPersonRequest.class));

    }

    /**
     * Test find person
     *
     * @throws Exception
     */
    @Test
    public void testFindPersonByFirstName() throws Exception {
        mockMvc
            .perform((get("/baseServices/api/v1/person/findPersonsByFirstName")
                .contextPath("/baseServices")).param("firstName", firstNameMock)
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk()).andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$[0].firstName", is(firstNameMock)))
            .andDo(MockMvcResultHandlers.print());
    }

}
