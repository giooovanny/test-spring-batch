package ec.com.smx.base.batch.linemaper;

import ec.com.smx.base.vo.batch.PersonFileLine;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * Personalized mapLine.
 *
 * @author gcadena on 23/3/20.
 * @version 1.0
 * @since 1.
 */
@Lazy
@Component
@StepScope
public class PersonLineMapper extends DefaultLineMapper<PersonFileLine> {


    @Override
    public final PersonFileLine mapLine(String line, int lineNumber) throws Exception {
        return super.mapLine(line, lineNumber);
    }

}
