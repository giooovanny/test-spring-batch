package ec.com.smx.base.common;

import org.hibernate.tool.schema.spi.SchemaFilterProvider;

public class SchemaProvider implements SchemaFilterProvider {

    @Override
    public org.hibernate.tool.schema.spi.SchemaFilter getCreateFilter() {
        return SchemaFilter.INSTANCE;
    }

    @Override
    public org.hibernate.tool.schema.spi.SchemaFilter getDropFilter() {
        return SchemaFilter.INSTANCE;
    }

    @Override
    public org.hibernate.tool.schema.spi.SchemaFilter getMigrateFilter() {
        return SchemaFilter.INSTANCE;
    }

    @Override
    public org.hibernate.tool.schema.spi.SchemaFilter getValidateFilter() {
        return SchemaFilter.INSTANCE;
    }
}
