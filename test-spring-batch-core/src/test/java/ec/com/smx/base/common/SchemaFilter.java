package ec.com.smx.base.common;

import org.hibernate.boot.model.relational.Namespace;
import org.hibernate.boot.model.relational.Sequence;
import org.hibernate.mapping.Table;

public class SchemaFilter implements org.hibernate.tool.schema.spi.SchemaFilter {

    public static final SchemaFilter INSTANCE = new SchemaFilter();

    @Override
    public boolean includeNamespace(Namespace namespace) {
        return true;
    }

    @Override
    public boolean includeTable(Table table) {
        if (table.getName().toLowerCase().contains("<lower table name to exclude>")) {
            return false;
        }
        return true;
    }

    @Override
    public boolean includeSequence(Sequence sequence) {
        return true;
    }
}
