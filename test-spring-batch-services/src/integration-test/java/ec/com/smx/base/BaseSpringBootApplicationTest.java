package ec.com.smx.base;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Test main spring boot app.
 *
 * @version 1.0
 * @autor klopez on 3/1/19.
 * @since 1.0.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BaseSpringBootApplication.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@AutoConfigureWireMock(port = 0)
// random port, that is wired into properties with key wiremock.server.port
public class BaseSpringBootApplicationTest {

    @Autowired
    private PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer;

    @Test
    public void testApplicationContextLoaded() {
        assertThat(propertySourcesPlaceholderConfigurer).isNotNull();
    }

    @Test
    public void testApplicationStarts() {
        BaseSpringBootApplication.main("--spring.main.web-environment=false",
            " -Dspring-boot.run.jvmArguments=\"-Ddebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5005\"");
        assertThat(propertySourcesPlaceholderConfigurer).isNotNull();
    }
}
