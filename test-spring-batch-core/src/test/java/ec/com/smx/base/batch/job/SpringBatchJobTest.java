package ec.com.smx.base.batch.job;

import ec.com.smx.base.config.BaseConfiguration;
import ec.com.smx.base.config.BatchConfiguration;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.*;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.batch.test.context.SpringBatchTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Test for job ans step.
 *
 * @author gcadena on 26/03/21.
 * @version 1.0
 * @since 1.0.0
 */
@SpringBatchTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {BaseConfiguration.class, BatchConfiguration.class})
public class SpringBatchJobTest {

    @Autowired
    private JobLauncherTestUtils jobLauncherTestUtils;

    private JobParameters defaultJobParameters() {
        JobParametersBuilder paramsBuilder = new JobParametersBuilder();
        paramsBuilder.addString("file.input", "batch/dat.csv");
        return paramsBuilder.toJobParameters();
    }

    //Test sin funcionamiento
    @Test
    public void testExchangeFilesJob() throws Exception {

        JobExecution jobExecution = jobLauncherTestUtils.launchJob(defaultJobParameters());
        JobInstance actualJobInstance = jobExecution.getJobInstance();
        ExitStatus actualJobExitStatus = jobExecution.getExitStatus();

        Assert.assertEquals("exchangeFilesJob",actualJobInstance.getJobName());
        Assert.assertEquals("COMPLETED",actualJobExitStatus.getExitCode());
    }

}
