package ec.com.smx.base.controller;

import java.util.List;
import ec.com.kruger.spring.ws.controller.BaseController;
import ec.com.smx.base.service.IPersonService;
import ec.com.smx.base.vo.FindPersonRequest;
import ec.com.smx.base.vo.FindPersonResponse;
import ec.com.smx.base.vo.FindPersonResponseV2;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Class to controller person v2 rest services.
 */
@RestController
@RequestMapping("/api/v2/person")
@Lazy
@Slf4j
@Tag(name = "Person", description = "The Person API v2")
public class PersonControllerV2 extends BaseController {

    @Lazy
    @Autowired
    @Getter
    private IPersonService personService;

    /**
     * Find person by some prefefields.
     *
     * @param person The person filter
     * @return Array of {@link FindPersonResponse}
     */
    @PostMapping(path = "/findPersons",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Get Persons by criteria")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Found Persons",
            content = { @Content(mediaType = "application/json",
                schema = @Schema(implementation = FindPersonResponseV2.class)) }),
        @ApiResponse(responseCode = "400", description = "Invalid filter",
            content = @Content),
        @ApiResponse(responseCode = "404", description = "Persons not found",
            content = @Content) })
    public List<FindPersonResponseV2> findPersons(@RequestBody FindPersonRequest person) {
        return this.personService.findSimplePersons(person);
    }
}
