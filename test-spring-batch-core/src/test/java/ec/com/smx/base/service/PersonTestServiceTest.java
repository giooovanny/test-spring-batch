package ec.com.smx.base.service;

import ec.com.smx.base.entity.PersonEntityTest;
import ec.com.smx.base.repository.PersonTestRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.Silent.class)
@Slf4j
public class PersonTestServiceTest {

    private PersonEntityTest personEntityTest;

    @InjectMocks
    private PersonTestService personTestService;

    @Mock
    private PersonTestRepository personTestRepository;

    @Test
    public void testSaveNotSave() {

        //ARRANGE
        personEntityTest = PersonEntityTest.builder().codePerson(4697234).identifyType("Cedulas").
                identifyNumber("73462349").firstName("Giovanny").lastName("Cadena").fullName("Giovanny Cadena").
                statusString("Soltero").gender("Masculino").build();
        //ACT
        personTestService.save(personEntityTest);

        //ASSERT
        verify(personTestRepository, times(1)).save(personEntityTest);

    }

}
