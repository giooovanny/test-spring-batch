package ec.com.smx.base.service;

import java.util.List;
import ec.com.smx.base.vo.FindPersonResponseV2;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import ec.com.kruger.spring.service.jpa.BaseService;
import ec.com.smx.base.entity.PersonEntity;
import ec.com.smx.base.exception.IdentifyNumberException;
import ec.com.smx.base.repository.IPersonRepository;
import ec.com.smx.base.vo.FindPersonRequest;
import ec.com.smx.base.vo.FindPersonResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Person service implementation.
 *
 * @author klopez on 2/20/19.
 * @version 1.0
 * @since 1.0.0
 */
@Transactional("jpaTransactionManager")
@Validated
@Lazy
@Service
public class PersonService extends BaseService<PersonEntity, IPersonRepository> implements
    IPersonService {

    /**
     * Constructor with dependencies.
     *
     * @param repository The repository to inject
     */
    public PersonService(IPersonRepository repository) {
        super(repository);
    }

    /**
     * {@inheritDoc}
     */
    @Transactional(readOnly = true)
    @Override
    public List<PersonEntity> findByFirstName(String firstName) {
        return repository.findByFirstName(firstName);
    }

    /**
     * {@inheritDoc}
     */
    @Transactional(readOnly = true, value = "jpaTransactionManager")
    @Override
    public List<FindPersonResponse> findPersons(FindPersonRequest personRequest) {
        return repository.findPersons(personRequest);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveWithValidation(@NotNull final PersonEntity personEntity) {
        final PersonEntity entity = repository
            .findByIdentifyNumber(personEntity.getIdentifyNumber());
        if (entity != null) {
            throw new IdentifyNumberException("Entity with identify number already exist");
        }
        repository.save(personEntity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FindPersonResponseV2> findSimplePersons(@Valid FindPersonRequest personRequest) {
        return this.repository.findSimplePersons(personRequest);
    }
}
